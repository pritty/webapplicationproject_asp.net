﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProject.Models
{
    [Serializable]
    public class ProductModel
    {
        public int Id { get; set; }
        public string ProductTitle { get; set; }
        public string ProductImageHeading { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageName { get; set; }
        public string ProductImageUrl { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }

    public class FetchProduct
    {
        public product Product { get; set; } 
        public string BaseStringData { get; set; } 
    }
}