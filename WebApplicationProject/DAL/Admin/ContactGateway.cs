﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class ContactGateway:ConnectionGateway
    {
        public int SaveContact(contact contactObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into contact(Heading,Address,Address2,Phone,Phone2,Mobile,Mobile2,MapUrl,CreatedAt,Status)
                        Values(@Heading,@Address,@Address2,@Phone,@Phone2,@Mobile,@Mobile2,@MapUrl,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Heading", contactObj.Heading);
                Command.Parameters.AddWithValue("@Address", contactObj.Address);
                Command.Parameters.AddWithValue("@Address2", contactObj.Address2);
                Command.Parameters.AddWithValue("@Phone", contactObj.Phone);
                Command.Parameters.AddWithValue("@Phone2", contactObj.Phone2);
                Command.Parameters.AddWithValue("@Mobile", contactObj.Mobile);
                Command.Parameters.AddWithValue("@Mobile2", contactObj.Mobile2);
                Command.Parameters.AddWithValue("@MapUrl", contactObj.MapUrl);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateContact(contact contactObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update contact set Heading=@Heading,Address=@Address,Address2=@Address2,Phone=@Phone,Phone2=@Phone2,Mobile=@Mobile,Mobile2=@Mobile2,MapUrl=@MapUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Heading", contactObj.Heading);
                Command.Parameters.AddWithValue("@Address", contactObj.Address);
                Command.Parameters.AddWithValue("@Address2", contactObj.Address2);
                Command.Parameters.AddWithValue("@Phone", contactObj.Phone);
                Command.Parameters.AddWithValue("@Phone2", contactObj.Phone2);
                Command.Parameters.AddWithValue("@Mobile", contactObj.Mobile);
                Command.Parameters.AddWithValue("@Mobile2", contactObj.Mobile2);
                Command.Parameters.AddWithValue("@MapUrl", contactObj.MapUrl);
                Command.Parameters.AddWithValue("@id", contactObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<contact> GetAllContactItemList()
        {
            try
            {
                List<contact> abouts = new List<contact>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from contact where Status=@Status";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    contact aData = new contact();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.Heading = Reader["Heading"].ToString();
                    aData.Address = Reader["Address"].ToString();
                    aData.Address2 = Reader["Address2"].ToString();
                    aData.Phone = Reader["Phone"].ToString();
                    aData.Phone2 = Reader["Phone2"].ToString();
                    aData.Mobile = Reader["Mobile"].ToString();
                    aData.Mobile2 = Reader["Mobile2"].ToString();
                    aData.MapUrl = Reader["MapUrl"].ToString();
                    abouts.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return abouts;
            }
            catch
            {
                Reader.Close();
                return new List<contact>();
            }
        }
        public int DeleteContact(int aboutId)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from contact where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", aboutId);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}