﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class AboutGateway:ConnectionGateway
    {
        public int SaveAbout(about aboutObj)
        {
            try
            {
                about aData = new about();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into about(CompanyDescription,Mission,Vission,CreatedAt,ModifiedAt,Status)Values(@CompanyDescription,@Mission,@Vission,GETDATE(),@ModifiedAt,@Status)";
                Command = new SqlCommand(Query, Connection);
                
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@CompanyDescription", aboutObj.CompanyDescription);
                Command.Parameters.AddWithValue("@Mission", aboutObj.Mission);
                Command.Parameters.AddWithValue("@Vission", aboutObj.Vission);
                Command.Parameters.AddWithValue("@ModifiedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateAbout(about aboutObj)
        {
            try
            {
                about aData = new about();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update about set CompanyDescription=@CompanyDescription,Mission=@Mission,Vission=@Vission,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@CompanyDescription", aboutObj.CompanyDescription);
                Command.Parameters.AddWithValue("@Mission", aboutObj.Mission);
                Command.Parameters.AddWithValue("@Vission", aboutObj.Vission);
                Command.Parameters.AddWithValue("@id", aboutObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<about> GetAllAboutList()
        {
            try
            {
                List<about> abouts=new List<about>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from about where Status=@Status";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader= Command.ExecuteReader();
                while (Reader.Read())
                {
                    about aData=new about();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.CompanyDescription = Reader["CompanyDescription"].ToString();
                    aData.Mission = Reader["Mission"].ToString();
                    aData.Vission = Reader["Vission"].ToString();
                    abouts.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return abouts;
            }
            catch
            {
                Reader.Close();
                return new List<about>();
            }
        }
        public int DeleteAbout(int aboutId)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from about where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", aboutId);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}