﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class LogoGateway : ConnectionGateway
    {
        public int SaveLogo(logo logoObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into logo(MenuId,LogoImage,LogoImageBinary,LogoImageName,LogoImageUrl,CreatedAt,Status,DeletedAt)
                        Values(@MenuId,@LogoImage,@LogoImageBinary,@LogoImageName,@LogoImageUrl,GETDATE(),@Status,@DeletedAt)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@MenuId", logoObj.MenuId);
                Command.Parameters.AddWithValue("@LogoImage", logoObj.LogoImage);
                Command.Parameters.AddWithValue("@LogoImageBinary", System.Convert.ToByte( logoObj.LogoImageBinary.Length));
                Command.Parameters.AddWithValue("@LogoImageName", logoObj.LogoImageName);
                Command.Parameters.AddWithValue("@LogoImageUrl", logoObj.LogoImageUrl);
                Command.Parameters.AddWithValue("@Status", 'A');
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateLogo(logo logoObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update logo set MenuId=@MenuId,LogoImage=@LogoImage,LogoImageBinary=@LogoImageBinary,LogoImageName=@LogoImageName,LogoImageUrl=@LogoImageUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@MenuId", logoObj.MenuId);
                Command.Parameters.AddWithValue("@LogoImage", logoObj.LogoImage);
                Command.Parameters.AddWithValue("@LogoImageBinary", System.Convert.ToByte(logoObj.LogoImageBinary.Length));
                Command.Parameters.AddWithValue("@LogoImageName", logoObj.LogoImageName);
                Command.Parameters.AddWithValue("@LogoImageUrl", logoObj.LogoImageUrl);
                Command.Parameters.AddWithValue("@id", logoObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<logo> GetAllLogoList()
        {
            try
            {
                List<logo> aList = new List<logo>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from logo where DeletedAt=@DeletedAt and Status=@Status";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    logo aData = new logo();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.MenuId = Convert.ToInt32(Reader["MenuId"].ToString());
                    aData.LogoImage = Reader["LogoImage"].ToString();
                    aData.LogoImageName = Reader["LogoImageName"].ToString();
                    aData.LogoImageUrl = Reader["LogoImageUrl"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<logo>();
            }
        }
        public int DeleteALogo(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from logo where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}