﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class ChatMessageGateway:ConnectionGateway
    {
        public int SaveChatMessage(ChatMesageBox chatObj)
        {
            try
            {
                ChatMesageBox aData = new ChatMesageBox();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into ChatMesageBox(QuestionCategory,Question,PersonName,ContactType,Email,ContactNo,CreatedAt,Status)
             Values(@QuestionCategory,@Question,@PersonName,@ContactType,@Email,@ContactNo,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);
                
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@QuestionCategory", chatObj.QuestionCategory);
                Command.Parameters.AddWithValue("@Question", chatObj.Question);
                Command.Parameters.AddWithValue("@PersonName", chatObj.PersonName);
                Command.Parameters.AddWithValue("@ContactType", chatObj.ContactType);
                Command.Parameters.AddWithValue("@Email", (object)chatObj.Email ?? DBNull.Value);
                Command.Parameters.AddWithValue("@ContactNo", (object)chatObj.ContactNo ?? DBNull.Value);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }

        public List<ChatMesageBox> GetAllChatbotMessageList()
        {
            try
            {
                List<ChatMesageBox> abouts = new List<ChatMesageBox>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from ChatMesageBox where Status=@Status";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader= Command.ExecuteReader();
                while (Reader.Read())
                {
                    ChatMesageBox aData = new ChatMesageBox();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.QuestionCategory = Reader["QuestionCategory"].ToString();
                    aData.Question = Reader["Question"].ToString();
                    aData.PersonName = Reader["PersonName"].ToString();
                    aData.ContactType = Reader["ContactType"].ToString();
                    aData.Email = Reader["Email"].ToString();
                    aData.ContactNo = Reader["ContactNo"].ToString();
                    abouts.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return abouts;
            }
            catch
            {
                Reader.Close();
                return new List<ChatMesageBox>();
            }
        }
        public int DeleteAbout(int aboutId)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from ChatMesageBox where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", aboutId);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}