﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class MenuGateway : ConnectionGateway
    {
        public int SaveMenu(menu menuObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into menu(MenuTitle,MenuUrl,CreatedAt,DeletedAt,Status)
                        Values(@MenuTitle,@MenuUrl,GETDATE(),@DeletedAt,@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@MenuTitle", menuObj.MenuTitle);
                Command.Parameters.AddWithValue("@MenuUrl", menuObj.MenuUrl);
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Command.Parameters.AddWithValue("@Status", 'A');

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateMenu(menu menuObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update menu set MenuTitle=@MenuTitle,MenuUrl=@MenuUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@MenuTitle", menuObj.MenuTitle);
                Command.Parameters.AddWithValue("@MenuUrl", menuObj.MenuUrl);
                Command.Parameters.AddWithValue("@id", menuObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<menu> GetAllMenuItemList()
        {
            try
            {
                List<menu> aList = new List<menu>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from menu where DeletedAt=@DeletedAt and Status=@Status";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    menu aData = new menu();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.MenuTitle = Reader["MenuTitle"].ToString();
                    aData.MenuUrl = Reader["MenuUrl"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<menu>();
            }
        }
        public int DeleteAMenu(int aboutId)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from menu where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", aboutId);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}