﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class SalesPointGateway : ConnectionGateway
    {
        public int SaveSalesPoint(SalesPoint salesPointObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into SalesPoint(BranchId,BranchName,Address,ManagerName,Mobile,Phone,WebAddress,Email,MapUrl,CreatedAt,Status)
                        Values(@BranchId,@BranchName,@Address,@ManagerName,@Mobile,@Mobile,@WebAddress,@Email,@MapUrl,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@BranchId", salesPointObj.BranchId);
                Command.Parameters.AddWithValue("@BranchName", salesPointObj.BranchName);
                Command.Parameters.AddWithValue("@Address", salesPointObj.Address);
                Command.Parameters.AddWithValue("@ManagerName", salesPointObj.ManagerName);
                Command.Parameters.AddWithValue("@Mobile", salesPointObj.Mobile);
                Command.Parameters.AddWithValue("@Phone", salesPointObj.Phone);
                Command.Parameters.AddWithValue("@WebAddress", salesPointObj.WebAddress);
                Command.Parameters.AddWithValue("@Email", salesPointObj.Email);
                Command.Parameters.AddWithValue("@MapUrl", salesPointObj.MapUrl);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateSalesPoint(SalesPoint salesPointObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update SalesPoint set BranchId=@BranchId,BranchName=@BranchName,Address=@Address,ManagerName=@ManagerName,Mobile=@Mobile,Phone=@Phone,WebAddress=@WebAddress,Email=@Email,MapUrl=@MapUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@BranchId", salesPointObj.BranchId);
                Command.Parameters.AddWithValue("@BranchName", salesPointObj.BranchName);
                Command.Parameters.AddWithValue("@Address", salesPointObj.Address);
                Command.Parameters.AddWithValue("@ManagerName", salesPointObj.ManagerName);
                Command.Parameters.AddWithValue("@Mobile", salesPointObj.Mobile);
                Command.Parameters.AddWithValue("@Phone", salesPointObj.Phone);
                Command.Parameters.AddWithValue("@WebAddress", salesPointObj.WebAddress);
                Command.Parameters.AddWithValue("@Email", salesPointObj.Email);
                Command.Parameters.AddWithValue("@MapUrl", salesPointObj.MapUrl);
                Command.Parameters.AddWithValue("@id", salesPointObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<SalesPoint> GetAllSalesPointList()
        {
            try
            {
                List<SalesPoint> abouts = new List<SalesPoint>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from SalesPoint where Status=@Status order by Id desc";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    SalesPoint aData = new SalesPoint();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.BranchId = Convert.ToInt32(Reader["BranchId"].ToString());
                    aData.BranchName = Reader["BranchName"].ToString();
                    aData.Address = Reader["Address"].ToString();
                    aData.ManagerName = Reader["ManagerName"].ToString();
                    aData.Phone = Reader["Phone"].ToString();
                    aData.Mobile = Reader["Mobile"].ToString();
                    aData.WebAddress = Reader["WebAddress"].ToString();
                    aData.Email = Reader["Email"].ToString();
                    aData.MapUrl = Reader["MapUrl"].ToString();
                    abouts.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return abouts;
            }
            catch
            {
                Reader.Close();
                return new List<SalesPoint>();
            }
        }
        public int DeleteSalesPoint(int Id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from SalesPoint where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", Id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}