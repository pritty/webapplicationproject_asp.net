﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class VideoGalleryGateway : ConnectionGateway
    {
        public int SaveVideoGallery(VideoGallery galleryObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into VideoGallery(Title,Description,VideoUrl,CreatedAt,DeletedAt,Status)
                        Values(@Title,@Description,@VideoUrl,GETDATE(),@DeletedAt,@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Title", galleryObj.Title);
                Command.Parameters.AddWithValue("@Description", galleryObj.Description);
                Command.Parameters.AddWithValue("@VideoUrl", galleryObj.VideoUrl);
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Command.Parameters.AddWithValue("@Status", "A");
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateGallery(VideoGallery galleryObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update VideoGallery set Title=@Title,Description=@Description,VideoUrl=@VideoUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Title", galleryObj.Title);
                Command.Parameters.AddWithValue("@Description", galleryObj.Description);
                Command.Parameters.AddWithValue("@VideoUrl", galleryObj.VideoUrl);
                Command.Parameters.AddWithValue("@id", galleryObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<VideoGallery> GetAllGalleryList()
        {
            try
            {
                List<VideoGallery> abouts = new List<VideoGallery>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from VideoGallery where DeletedAt=@DeletedAt and Status=@Status";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Command.Parameters.AddWithValue("@Status", "A");
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    VideoGallery aData = new VideoGallery();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.Title = Reader["Title"].ToString();
                    aData.Description = Reader["Description"].ToString();
                    aData.VideoUrl = Reader["VideoUrl"].ToString();
                    abouts.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return abouts;
            }
            catch
            {
                Reader.Close();
                return new List<VideoGallery>();
            }
        }
        public int DeleteGallery(int videoId)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from VideoGallery where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", videoId);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}