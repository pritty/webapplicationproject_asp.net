﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class CareerGateway : ConnectionGateway
    {
        public int SaveCareer(Career careerObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into Career(Designation,Requirement,Description,Address,ContactPersonName,ContactPersonDetails,Mobile,Phone,Email,ApplicationStartDate,ApplicationLastDate,CreatedAt,Status)
Values(@Designation,@Requirement,@Description,@Address,@ContactPersonName,@ContactPersonDetails,@Mobile,@Phone,@Email,@ApplicationStartDate,@ApplicationLastDate,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);
                
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Designation", careerObj.Designation);
                Command.Parameters.AddWithValue("@Requirement", careerObj.Requirement);
                Command.Parameters.AddWithValue("@Description", careerObj.Description);
                Command.Parameters.AddWithValue("@Address", careerObj.Address);
                Command.Parameters.AddWithValue("@ContactPersonName", careerObj.ContactPersonName);
                Command.Parameters.AddWithValue("@ContactPersonDetails", careerObj.ContactPersonDetails);
                Command.Parameters.AddWithValue("@Mobile", careerObj.Mobile);
                Command.Parameters.AddWithValue("@Phone", careerObj.Phone);
                Command.Parameters.AddWithValue("@Email", careerObj.Email);
                Command.Parameters.AddWithValue("@ApplicationStartDate", careerObj.ApplicationStartDate);
                Command.Parameters.AddWithValue("@ApplicationLastDate", careerObj.ApplicationLastDate);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateCareer(Career careerObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update Career set Designation=@Designation,Requirement=@Requirement,Description=@Description,Address=@Address,ContactPersonName=@ContactPersonName,ContactPersonDetails=@ContactPersonDetails,
Mobile=@Mobile,Phone=@Phone,Email=@Email,ApplicationStartDate=@ApplicationStartDate,ApplicationLastDate=@ApplicationLastDate,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Designation", careerObj.Designation);
                Command.Parameters.AddWithValue("@Requirement", careerObj.Requirement);
                Command.Parameters.AddWithValue("@Description", careerObj.Description);
                Command.Parameters.AddWithValue("@Address", careerObj.Address);
                Command.Parameters.AddWithValue("@ContactPersonName", careerObj.ContactPersonName);
                Command.Parameters.AddWithValue("@ContactPersonDetails", careerObj.ContactPersonDetails);
                Command.Parameters.AddWithValue("@Mobile", careerObj.Mobile);
                Command.Parameters.AddWithValue("@Phone", careerObj.Phone);
                Command.Parameters.AddWithValue("@Email", careerObj.Email);
                Command.Parameters.AddWithValue("@ApplicationStartDate", careerObj.ApplicationStartDate);
                Command.Parameters.AddWithValue("@ApplicationLastDate", careerObj.ApplicationLastDate);
                Command.Parameters.AddWithValue("@id", careerObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<Career> GetAllCareerList()
        {
            try
            {
                List<Career> aList = new List<Career>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from Career where Status=@Status";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader= Command.ExecuteReader();
                while (Reader.Read())
                {
                    Career aData = new Career();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.Designation = Reader["Designation"].ToString();
                    aData.Requirement = Reader["Requirement"].ToString();
                    aData.Description = Reader["Description"].ToString();
                    aData.Address = Reader["Address"].ToString();
                    aData.ContactPersonName = Reader["ContactPersonName"].ToString();
                    aData.ContactPersonDetails = Reader["ContactPersonDetails"].ToString();
                    aData.Mobile = Reader["Mobile"].ToString();
                    aData.Phone = Reader["Phone"].ToString();
                    aData.Email = Reader["Email"].ToString();
                    aData.ApplicationStartDate = Reader["ApplicationStartDate"].ToString();
                    aData.ApplicationLastDate = Reader["ApplicationLastDate"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<Career>();
            }
        }
        public int DeleteCareer(int aboutId)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from Career where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", aboutId);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}