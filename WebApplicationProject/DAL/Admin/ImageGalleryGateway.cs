﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class ImageGalleryGateway : ConnectionGateway
    {
        public int SaveGalley(ImageGallery galleryObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into ImageGallery(Title,ImageStringData,ImageName,ImageUrl,Description,Status,CreatedAt,DeletedAt)
                        Values(@Title,@ImageStringData,@ImageName,@ImageUrl,@Description,@Status,GETDATE(),@DeletedAt)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Title", galleryObj.Title);
                Command.Parameters.AddWithValue("@ImageStringData", galleryObj.ImageStringData);
                Command.Parameters.AddWithValue("@ImageName", galleryObj.ImageName);
                Command.Parameters.AddWithValue("@ImageUrl", galleryObj.ImageUrl);
                Command.Parameters.AddWithValue("@Description", galleryObj.Description);
                Command.Parameters.AddWithValue("@Status", 'A');
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateGallery(ImageGallery galleryObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update ImageGallery set Title=@Title,ImageStringData=@ImageStringData,ImageName=@ImageName,ImageUrl=@ImageUrl,Description=@Description,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Title", galleryObj.Title);
                Command.Parameters.AddWithValue("@ImageStringData", galleryObj.ImageStringData);
                Command.Parameters.AddWithValue("@ImageName", galleryObj.ImageName);
                Command.Parameters.AddWithValue("@ImageUrl", galleryObj.ImageUrl);
                Command.Parameters.AddWithValue("@Description", galleryObj.Description);
                Command.Parameters.AddWithValue("@id", galleryObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<ImageGallery> GetAllGalleryList()
        {
            try
            {
                List<ImageGallery> aList = new List<ImageGallery>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from ImageGallery where DeletedAt=@DeletedAt and Status=@Status";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Command.Parameters.AddWithValue("@Status","A");
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    ImageGallery aData = new ImageGallery();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.Title = Reader["Title"].ToString();
                    aData.ImageStringData = Reader["ImageStringData"].ToString();
                    aData.ImageName = Reader["ImageName"].ToString();
                    aData.ImageUrl = Reader["ImageUrl"].ToString();
                    aData.Description = Reader["Description"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<ImageGallery>();
            }
        }
        public int DeleteAGallery(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from ImageGallery where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}