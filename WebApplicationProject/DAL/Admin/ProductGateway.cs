﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class ProductGateway:ConnectionGateway
    {
        public int SaveProduct(product productObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into product(ProductTitle,ProductImageHeading,ProductImage,ProductImageBinary,ProductImageName,ProductImageUrl,Description,CreatedAt,Status)
                        Values(@ProductTitle,@ProductImageHeading,@ProductImage,@ProductImageBinary,@ProductImageName,@ProductImageUrl,@Description,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@ProductTitle", productObj.ProductTitle);
                Command.Parameters.AddWithValue("@ProductImageHeading", productObj.ProductImageHeading);
                Command.Parameters.AddWithValue("@ProductImage", productObj.ProductImage);
                Command.Parameters.AddWithValue("@ProductImageBinary", System.Convert.ToByte( productObj.ProductImageBinary.Length));
                Command.Parameters.AddWithValue("@ProductImageName", productObj.ProductImageName);
                Command.Parameters.AddWithValue("@ProductImageUrl", productObj.ProductImageUrl);
                Command.Parameters.AddWithValue("@Description", productObj.Description);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateProduct(product productObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update product set ProductTitle=@ProductTitle,ProductImageHeading=@ProductImageHeading,ProductImage=@ProductImage,ProductImageBinary=@ProductImageBinary,ProductImageName=@ProductImageName,ProductImageUrl=@ProductImageUrl,Description=@Description,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@ProductTitle", productObj.ProductTitle);
                Command.Parameters.AddWithValue("@ProductImageHeading", productObj.ProductImageHeading);
                Command.Parameters.AddWithValue("@ProductImage", productObj.ProductImage);
                Command.Parameters.AddWithValue("@ProductImageBinary", System.Convert.ToByte(productObj.ProductImageBinary.Length));
                Command.Parameters.AddWithValue("@ProductImageName", productObj.ProductImageName);
                Command.Parameters.AddWithValue("@ProductImageUrl", productObj.ProductImageUrl);
                Command.Parameters.AddWithValue("@Description", productObj.Description);
                Command.Parameters.AddWithValue("@id", productObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<product> GetAllProductList()
        {
            try
            {
                List<product> aList = new List<product>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from product where Status=@Status order by Id desc";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    product aData = new product();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.ProductTitle = Reader["ProductTitle"].ToString();
                    aData.ProductImageHeading = Reader["ProductImageHeading"].ToString();
                    aData.ProductImage = Reader["ProductImage"].ToString();
                    aData.ProductImageName = Reader["ProductImageName"].ToString();
                    aData.ProductImageUrl = Reader["ProductImageUrl"].ToString();
                    aData.Description = Reader["Description"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<product>();
            }
        }
        public int DeleteAProduct(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from product where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}