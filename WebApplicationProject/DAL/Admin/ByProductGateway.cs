﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class ByProductGateway : ConnectionGateway
    {
        public int SaveProduct(ByProduct productObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into ByProduct(ProductTitle,ProductImageHeading,ProductImage,ProductImageBinary,ProductImageName,ProductImageUrl,Description,CreatedAt,Status,DeletedAt)
                        Values(@ProductTitle,@ProductImageHeading,@ProductImage,@ProductImageBinary,@ProductImageName,@ProductImageUrl,@Description,GETDATE(),@Status,@DeletedAt)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@ProductTitle", productObj.ProductTitle);
                Command.Parameters.AddWithValue("@ProductImageHeading", productObj.ProductImageHeading);
                Command.Parameters.AddWithValue("@ProductImage", productObj.ProductImage);
                Command.Parameters.AddWithValue("@ProductImageBinary", System.Convert.ToByte( productObj.ProductImageBinary.Length));
                Command.Parameters.AddWithValue("@ProductImageName", productObj.ProductImageName);
                Command.Parameters.AddWithValue("@ProductImageUrl", productObj.ProductImageUrl);
                Command.Parameters.AddWithValue("@Description", productObj.Description);
                Command.Parameters.AddWithValue("@Status", 'A');
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateProduct(ByProduct productObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update ByProduct set ProductTitle=@ProductTitle,ProductImageHeading=@ProductImageHeading,ProductImage=@ProductImage,ProductImageBinary=@ProductImageBinary,ProductImageName=@ProductImageName,ProductImageUrl=@ProductImageUrl,Description=@Description,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@ProductTitle", productObj.ProductTitle);
                Command.Parameters.AddWithValue("@ProductImageHeading", productObj.ProductImageHeading);
                Command.Parameters.AddWithValue("@ProductImage", productObj.ProductImage);
                Command.Parameters.AddWithValue("@ProductImageBinary", System.Convert.ToByte(productObj.ProductImageBinary.Length));
                Command.Parameters.AddWithValue("@ProductImageName", productObj.ProductImageName);
                Command.Parameters.AddWithValue("@ProductImageUrl", productObj.ProductImageUrl);
                Command.Parameters.AddWithValue("@Description", productObj.Description);
                Command.Parameters.AddWithValue("@id", productObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<ByProduct> GetAllProductList()
        {
            try
            {
                List<ByProduct> aList = new List<ByProduct>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from ByProduct where DeletedAt=@DeletedAt and Status=@Status order by Id desc";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    ByProduct aData = new ByProduct();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.ProductTitle = Reader["ProductTitle"].ToString();
                    aData.ProductImageHeading = Reader["ProductImageHeading"].ToString();
                    aData.ProductImage = Reader["ProductImage"].ToString();
                    aData.ProductImageName = Reader["ProductImageName"].ToString();
                    aData.ProductImageUrl = Reader["ProductImageUrl"].ToString();
                    aData.Description = Reader["Description"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<ByProduct>();
            }
        }
        public int DeleteAProduct(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from ByProduct where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}