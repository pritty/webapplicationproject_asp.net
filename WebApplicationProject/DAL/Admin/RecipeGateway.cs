﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class RecipeGateway : ConnectionGateway
    {
        public int SaveRecipe(Recipe recipeObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into Recipe(Title,SubTitle,MadeByRecipie,ShortDescription,DetailsDescription,ImageName,ImageDataString,ImageUrl,VideoUrl,CreatedAt,Status)
                        Values(@Title,@SubTitle,@MadeByRecipie,@ShortDescription,@DetailsDescription,@ImageName,@ImageDataString,@ImageUrl,@VideoUrl,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Title", recipeObj.Title);
                Command.Parameters.AddWithValue("@SubTitle", recipeObj.SubTitle);
                Command.Parameters.AddWithValue("@MadeByRecipie", recipeObj.MadeByRecipie);
                Command.Parameters.AddWithValue("@ShortDescription", recipeObj.ShortDescription);
                Command.Parameters.AddWithValue("@DetailsDescription", recipeObj.DetailsDescription);
                Command.Parameters.AddWithValue("@ImageName", recipeObj.ImageName);
                Command.Parameters.AddWithValue("@ImageDataString", recipeObj.ImageDataString);
                Command.Parameters.AddWithValue("@ImageUrl", recipeObj.ImageUrl);
                Command.Parameters.AddWithValue("@VideoUrl", recipeObj.VideoUrl);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateRecipe(Recipe recipeObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update Recipe set Title=@Title,SubTitle=@SubTitle,MadeByRecipie=@MadeByRecipie,ShortDescription=@ShortDescription,DetailsDescription=@DetailsDescription,
ImageName=@ImageName,ImageDataString=@ImageDataString,ImageUrl=@ImageUrl,VideoUrl=@VideoUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Title", recipeObj.Title);
                Command.Parameters.AddWithValue("@SubTitle", recipeObj.SubTitle);
                Command.Parameters.AddWithValue("@MadeByRecipie", recipeObj.MadeByRecipie);
                Command.Parameters.AddWithValue("@ShortDescription", recipeObj.ShortDescription);
                Command.Parameters.AddWithValue("@DetailsDescription", recipeObj.DetailsDescription);
                Command.Parameters.AddWithValue("@ImageName", recipeObj.ImageName);
                Command.Parameters.AddWithValue("@ImageDataString", recipeObj.ImageDataString);
                Command.Parameters.AddWithValue("@ImageUrl", recipeObj.ImageUrl);
                Command.Parameters.AddWithValue("@VideoUrl", recipeObj.VideoUrl);
                Command.Parameters.AddWithValue("@id", recipeObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<Recipe> GetAllRecipeList()
        {
            try
            {
                List<Recipe> aList = new List<Recipe>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from Recipe where Status=@Status";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    Recipe aData = new Recipe();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.Title = Reader["Title"].ToString();
                    aData.SubTitle = Reader["SubTitle"].ToString();
                    aData.MadeByRecipie = Reader["MadeByRecipie"].ToString();
                    aData.ShortDescription = Reader["ShortDescription"].ToString();
                    aData.DetailsDescription = Reader["DetailsDescription"].ToString();
                    aData.ImageName = Reader["ImageName"].ToString();
                    aData.ImageDataString = Reader["ImageDataString"].ToString();
                    aData.ImageUrl = Reader["ImageUrl"].ToString();
                    aData.VideoUrl = Reader["VideoUrl"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<Recipe>();
            }
        }
        public int DeleteARecipe(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from Recipe where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}