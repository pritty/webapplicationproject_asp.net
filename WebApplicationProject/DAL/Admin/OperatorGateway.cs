﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplicationProject.DAL.Admin
{
    public class OperatorGateway:ConnectionGateway
    {
        public tblOperator Login(tblOperator atblOperator)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string query = "SELECT * FROM tblOperator WHERE UserId=@userId and Password=@pass";

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@userId", atblOperator.UserId);
                    cmd.Parameters.AddWithValue("@pass", atblOperator.Password);

                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        atblOperator.OperatorId = Convert.ToInt32(reader["OperatorId"].ToString());
                        atblOperator.FullName = reader["FullName"].ToString();
                        atblOperator.CounterId = Convert.ToInt32(reader["CounterId"].ToString());

                        atblOperator.OptMobile = reader["OptMobile"].ToString();
                        string s = reader["CreditLimit"].ToString();
                        s = s == "" ? "-100" : s; //-100 for no limit
                        atblOperator.CreditLimit = Convert.ToDouble(s);
                        atblOperator.Type = reader["Type"].ToString();
                        //atblOperator.Department = reader["Department"].ToString();
                        atblOperator.Department = reader["CounterId"].ToString();
                        atblOperator.BranchLocation = reader["BranchLocation"].ToString();
                        atblOperator.Company = reader["Company"].ToString();

                    }

                    reader.Close();
                    con.Close();

                    return atblOperator;
                }
            }
            catch (Exception e)
            {
                return atblOperator;
            }
        }

        public bool LogOut(tblOperator atblOperator)
        {
            try
            {
                using (SqlConnection con2 = new SqlConnection(ConnectionString))
                {
                    string query = "UPDATE tblOperator SET LoggedIn=@status WHERE OperatorId=@oId";

                    SqlCommand cmd = new SqlCommand(query, con2);

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@status", 'N');
                    cmd.Parameters.AddWithValue("@oId", atblOperator.OperatorId);

                    bool res = false;

                    con2.Open();

                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        res = true;
                    }

                    con2.Close();


                    return res;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckPageAccess(int oId, string pageName)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string query = "SELECT dbo.MenuTree.MenuUrl, dbo.MenuTree.Id, dbo.MenuControl.OperatorId FROM dbo.MenuTree INNER JOIN dbo.MenuControl ON dbo.MenuTree.Id = dbo.MenuControl.MenuId WHERE dbo.MenuControl.OperatorId = @operatorId and dbo.MenuTree.MenuUrl like @pageName";

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@operatorId", oId);
                    cmd.Parameters.AddWithValue("@pageName", "%" + pageName);

                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    string menuId = "";
                    if (reader.Read())
                    {
                        menuId = reader["Id"].ToString();
                    }

                    bool exists = !String.IsNullOrEmpty(menuId);

                    reader.Close();
                    con.Close();

                    return exists;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool CheckLogIn(tblOperator atblOperator)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string query = "Select * FROM tblOperator WHERE UserId=@userId and Password=@oPass";

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@userId", atblOperator.UserId);
                    cmd.Parameters.AddWithValue("@oPass", atblOperator.Password);


                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        atblOperator.FullName = reader["FullName"].ToString();
                    }

                    bool exists = atblOperator.FullName != null;

                    reader.Close();
                    con.Close();

                    return exists;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public int UpdatetblOperator(tblOperator atblOperator)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string query = "";
                    string opNameOld = "";

                    if (String.IsNullOrEmpty(atblOperator.Password))
                    {
                        //query = "UPDATE [dbo].[tblOperator] SET [FullName] = @FullName, [CounterId] = @CounterId, [OptMobile] = @OptMobile, [UserId] = @UserId, [Password] = @Password, [Type] = @Type, [Department] = @Department, [Company] = @Company, [BalanceDate] = @BalanceDate, [CreditLimit] = @CreditLimit, [OpeningBalance] = @OpeningBalance,[UserPic] = @UserPic,[LoggedIn] = @LoggedIn,[LoggedInIp] = @LoggedInIp,[State] = @State,[CookieState] = @CookieState WHERE OperatorId=@id";
                        query = "UPDATE [dbo].[tblOperator] SET [CounterId] = @CounterId WHERE OperatorId=@id";
                    }
                    else
                    {
                        query = "UPDATE [dbo].[tblOperator] SET [Password] = @Password WHERE OperatorId=@id";
                    }

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@CounterId", atblOperator.CounterId);
                    //cmd.Parameters.AddWithValue("@optMobile", atblOperator.OptMobile);
                    //cmd.Parameters.AddWithValue("@userId", atblOperator.UserId);
                    cmd.Parameters.AddWithValue("@Password", atblOperator.Password);
                    //cmd.Parameters.AddWithValue("@type", atblOperator.Type);
                    //cmd.Parameters.AddWithValue("@department", atblOperator.Department);
                    //cmd.Parameters.AddWithValue("@company", atblOperator.Company);

                    cmd.Parameters.AddWithValue("@id", atblOperator.OperatorId);

                    if (con.State == ConnectionState.Closed) { con.Open(); }

                    int rowEffected = cmd.ExecuteNonQuery();

                    if (con.State == ConnectionState.Open) { con.Close(); }


                    return rowEffected;
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }
    }
}