﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class SistersConcernGateway : ConnectionGateway
    {
        public int SaveSisterrsConcern(SisterrsConcern objConcern)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into SisterrsConcern(CompanyName,Address,Email,MobileNo1,MobileNo2,MobileNo3,CreatedTime,Status)
                        Values(@CompanyName,@Address,@Email,@MobileNo1,@MobileNo2,@MobileNo3,GETDATE(),@Status)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@CompanyName", objConcern.CompanyName);
                Command.Parameters.AddWithValue("@Address", objConcern.Address);
                Command.Parameters.AddWithValue("@Email", objConcern.Email);
                Command.Parameters.AddWithValue("@MobileNo1", objConcern.MobileNo1);
                Command.Parameters.AddWithValue("@MobileNo2", objConcern.MobileNo2);
                Command.Parameters.AddWithValue("@MobileNo3", objConcern.MobileNo3);
                Command.Parameters.AddWithValue("@Status", 'A');
                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateSisterrsConcern(SisterrsConcern objConcern)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update SisterrsConcern set CompanyName=@CompanyName,Address=@Address,Email=@Email,MobileNo1=@MobileNo1,MobileNo2=@MobileNo2,MobileNo3=@MobileNo3,ModifiedTime=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@CompanyName", objConcern.CompanyName);
                Command.Parameters.AddWithValue("@Address", objConcern.Address);
                Command.Parameters.AddWithValue("@Email", objConcern.Email);
                Command.Parameters.AddWithValue("@MobileNo1", objConcern.MobileNo1);
                Command.Parameters.AddWithValue("@MobileNo2", objConcern.MobileNo2);
                Command.Parameters.AddWithValue("@MobileNo3", objConcern.MobileNo3);
                Command.Parameters.AddWithValue("@id", objConcern.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<SisterrsConcern> GetAllSistersConcernList()
        {
            try
            {
                List<SisterrsConcern> aList = new List<SisterrsConcern>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from SisterrsConcern where Status=@Status";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Status", 'A');
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    SisterrsConcern aData = new SisterrsConcern();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.CompanyName = Reader["CompanyName"].ToString();
                    aData.Address = Reader["Address"].ToString();
                    aData.Email = Reader["Email"].ToString();
                    aData.MobileNo1 = Reader["MobileNo1"].ToString();
                    aData.MobileNo2 = Reader["MobileNo2"].ToString();
                    aData.MobileNo3 = Reader["MobileNo3"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<SisterrsConcern>();
            }
        }
        public int DeleteASistersConcern(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from SisterrsConcern where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}