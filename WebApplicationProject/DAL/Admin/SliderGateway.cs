﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplicationProject.Models;
using WebApplicationProject.UI.Admin;

namespace WebApplicationProject.DAL.Admin
{
    public class SliderGateway : ConnectionGateway
    {
        public int SaveSlider(slider sliderObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Insert into slider(SliderTitle,ImageName,ImageLarge,ImageLargeUrl,CreatedAt,DeletedAt)
                        Values(@SliderTitle,@ImageName,@ImageLarge,@ImageLargeUrl,GETDATE(),@DeletedAt)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@SliderTitle", sliderObj.SliderTitle);
                Command.Parameters.AddWithValue("@ImageName", sliderObj.ImageName);
                Command.Parameters.AddWithValue("@ImageLarge", sliderObj.ImageLarge);
                Command.Parameters.AddWithValue("@ImageLargeUrl", sliderObj.ImageLargeUrl);
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public int UpdateSlider(slider sliderObj)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Update slider set SliderTitle=@SliderTitle,ImageName=@ImageName,ImageLarge=@ImageLarge,ImageLargeUrl=@ImageLargeUrl,ModifiedAt=GETDATE() where Id=@id";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@SliderTitle", sliderObj.SliderTitle);
                Command.Parameters.AddWithValue("@ImageName", sliderObj.ImageName);
                Command.Parameters.AddWithValue("@ImageLarge", sliderObj.ImageLarge);
                Command.Parameters.AddWithValue("@ImageLargeUrl", sliderObj.ImageLargeUrl);
                Command.Parameters.AddWithValue("@id", sliderObj.Id);

                RowCount = Command.ExecuteNonQuery();

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
        public List<slider> GetAllSliderList()
        {
            try
            {
                List<slider> aList = new List<slider>();
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Select * from slider where DeletedAt=@DeletedAt";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@DeletedAt", (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
                Reader = Command.ExecuteReader();
                while (Reader.Read())
                {
                    slider aData = new slider();
                    aData.Id = Convert.ToInt32(Reader["Id"].ToString());
                    aData.SliderTitle = Reader["SliderTitle"].ToString();
                    //aData.ImageLarge = Reader["ImageLarge"].ToString();
                    aData.ImageName = Reader["ImageName"].ToString();
                    aData.ImageLargeUrl = Reader["ImageLargeUrl"].ToString();
                    aList.Add(aData);
                }
                Reader.Close();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return aList;
            }
            catch
            {
                Reader.Close();
                return new List<slider>();
            }
        }
        public int DeleteASlider(int id)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) { Connection.Open(); }
                Query = @"Delete from slider where Id=@id";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@id", id);

                RowCount = Command.ExecuteNonQuery();
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }

                return RowCount;
            }
            catch
            {
                return -1;
            }
        }
    }
}