﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class VideoGalleryUI : System.Web.UI.Page
    {
        public static VideoGalleryManager AVideoGalleryManager = new VideoGalleryManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string SaveVideoGallery(VideoGallery galleryObj)
        {
            return AVideoGalleryManager.SaveVideoGallery(galleryObj);
        } 
        
        [WebMethod]
        public static List<VideoGallery> GetAllGalleryList()
        {
            return AVideoGalleryManager.GetAllGalleryList();
        }
        [WebMethod]
        public static string DeleteGallery(int Id)
        {
            return AVideoGalleryManager.DeleteGallery(Id);
        }
    }
}