﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class MenuUI : System.Web.UI.Page
    {
        public static MenuManager AMenuManager = new MenuManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string SaveMenu(menu menuObj)
        {
            return AMenuManager.SaveMenu(menuObj);
        } 
        
        [WebMethod]
        public static List<menu> GetAllMenuItemList()
        {
            return AMenuManager.GetAllMenuItemList();
        }
        [WebMethod]
        public static string DeleteAMenu(int Id)
        {
            return AMenuManager.DeleteAMenu(Id);
        }
    }
}