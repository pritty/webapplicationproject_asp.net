﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class SliderUI : System.Web.UI.Page
    {
        public static SliderManager ASliderManager = new SliderManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<slider> GetAllSlider()
        {
            return ASliderManager.GetAllSlider();
        }
        
        [WebMethod]
        public static string DeleteASlider(int Id, string imgUrl)
        {
            return ASliderManager.DeleteASlider(Id, imgUrl);
        }
    }
}