﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CareerUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.CareerUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <!-- bootstrap wysihtml5 - text editor -->
    <style>
        .box-header {
	    background: #00c0ef;
    }
        .clearSpace{
            margin-top:6px;
        }
                .tableHeaderColor {
            background: #00c0ef;
        }
        .dataTables_wrapper {
	        padding: 5px;
	        margin: 5px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
       .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
     <!-- Message Box -->
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
        <!-- End Message Box -->
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Career Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                 <div class="form-group col-md-6">
                    <label class="control-label" for="designation">Designation:</label>
                        <input type="hidden"  name="varId" id="varId" value="" />
                        <input type="text" class="form-control" name="designation" id="designation" value="" />
                 </div>
                <div class="form-group col-md-3">
                    <label class="control-label" for="applicationStartDate">Application Start Date:</label>
                        <input type="text" class="form-control" name="applicationStartDate" id="applicationStartDate" value="" />
                 </div>
                <div class="form-group col-md-3">
                    <label class="control-label" for="applicationLastDate">Application Last Date:</label>
                        <input type="text" class="form-control" name="applicationLastDate" id="applicationLastDate" value="" />
                 </div>
                <div class="form-group">
                <label class="control-label" for="requirement">Requirement:</label>
                    <textarea class="form-control" id="requirement" name="requirement" placeholder="Requirement" rows="10" cols="80">
                    </textarea>
                </div>
                <div class="form-group">
                <label class="control-label" for="description">Description:</label>
                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="10" cols="80">
                    </textarea>
                </div>
                <div class="form-group col-md-3">
                    <label class="control-label" for="email">Email:</label>
                        <input type="text" class="form-control" name="email" id="email" value="" />
                 </div>
                <div class="form-group col-md-3">
                    <label class="control-label" for="contactPersonName">Contact Person Name:</label>
                        <input type="text" class="form-control" name="contactPersonName" id="contactPersonName" value="" />
                 </div>
                <div class="form-group col-md-3">
                    <label class="control-label" for="mobile">Mobile:</label>
                        <input type="text" class="form-control" name="mobile" id="mobile" value="" />
                 </div>
                <div class="form-group col-md-3">
                    <label class="control-label" for="phone">Phone:</label>
                        <input type="text" class="form-control" name="phone" id="phone" value="" />
                 </div>
                 <div class="form-group col-md-6">
                    <label class="control-label" for="contactPersonDetails">Contact Person Details:</label>
                     <textarea class="form-control" id="contactPersonDetails" name="contactPersonDetails" placeholder="Contact Person Details" rows="4" cols="80">
                    </textarea>
                 </div>
                <div class="form-group col-md-6">
                <label class="control-label" for="address">Address:</label>
                    <textarea class="form-control" id="address" name="address" placeholder="Address" rows="4" cols="80">
                    </textarea>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
<div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table id="gridViewTable" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead class="" style="background:#00c0ef">
                <tr role="row" class="">
                    <th>SL NO</th>
                    <th>Designation</th>
                    <th>Requirement</th>
                    <th>Description</th>
                    <th>Mobile</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>ApplicationLastDate</th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="aboutTableBody">                   
                </tbody>
              </table>

        </div>

    </div>

  </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
        <script src="../../Admin/jquery-3.6.0.min.js"></script>
    <script src="../../Admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- CK Editor -->
<script src="../../Admin/bower_components/ckeditor/ckeditor.js"></script>
 <!-- CK Editor -->
<script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            CKEDITOR.replace('requirement');
            CKEDITOR.replace('description');
        })
</script>
    <script src="../../Scripts/Admin/Career.js"></script>
</asp:Content>
