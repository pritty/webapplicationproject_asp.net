﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class CareerUI : System.Web.UI.Page
    {
        public static CareerManager ACareerManager = new CareerManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string SaveCareer(Career careerObj)
        {
            return ACareerManager.SaveCareer(careerObj);
        }
        
        [WebMethod]
        public static List<Career> GetAllCareerList()
        {
            return ACareerManager.GetAllCareerList();
        } 
        [WebMethod]
        public static string DeleteCareer(int Id)
        {
            return ACareerManager.DeleteCareer(Id);
        }
    }
}