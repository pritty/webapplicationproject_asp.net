﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SistersConcernUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.SistersConcernUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	    background: #00c0ef;
    }
        .clearSpace{
            margin-top:6px;
        }
                .tableHeaderColor {
            background: #00c0ef;
        }
        .dataTables_wrapper {
	        padding: 5px;
	        margin: 5px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
        table.dataTable thead .sorting, 
        table.dataTable thead .sorting_asc, 
        table.dataTable thead .sorting_desc {
            background : none;
        }
        .no-sort::after { display: none!important; }

        .no-sort { pointer-events: none!important; cursor: default!important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">SistersConcern Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="row">
                    <div class="form-group col-md-4">
                    <label class="control-label" for="companyName">Company Name:</label>
                        <input type="hidden"  name="varId" id="varId" value="" />
                        <input type="text" class="form-control" name="companyName" id="companyName" value="" />
                    </div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="email">Email:</label>
                        <input type="text" class="form-control" name="email" id="email" value="" />
                    </div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="address">Address:</label>
                        <textarea class="form-control" id="address" name="address" placeholder="Address" rows="4" cols="10">
                        </textarea>
                    </div> 
             </div>
             <div class="row">
                    <div class="form-group col-md-3">
                    <label class="control-label" for="mobile">Mobile:</label>
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="" />
                    </div>
                    <div class="form-group col-md-3">
                    <label class="control-label" for="mobile2">Mobile2:</label>
                        <input type="text" class="form-control" name="mobile2" id="mobile2" placeholder="Mobile2" value="" />
                    </div>
                    <div class="form-group col-md-3">
                    <label class="control-label" for="phone">Phone:</label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="" />
                    </div> 
<%--                    <div class="form-group">
                    <label class="control-label" for="productImage">Image:</label>
                        <input type="file" class="form-control" name="productImage" id="productImage" placeholder="productImage" value="" />
                        <input type="hidden"  id="imgUrl" value=""/>
                        <input type="hidden"  id="imgName" value=""/>
                    </div>
                   <div class="form-group">
                       <label class="control-label" for="">Preview Image:</label>
                         <img class="img-responsive"  style="height: 250px; width: 300px" id="prevImg"/>
                    </div>--%>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn" onclick="SaveSistersConcern();">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
             
              <div>
                <div class="col-md-12">
                     <table id="gridViewTable" class="table table-bordered table-striped">
                        <thead class="tableHeaderColor">
                        <tr>
                          <th class="no-sort">SL NO</th>
                          <th class="no-sort">CompanyName</th>
                          <th class="no-sort">Address</th>
                          <th class="no-sort">Email</th>
                          <th class="no-sort">MobileNo1</th>
                          <th class="no-sort">MobileNo2</th>
                          <th class="no-sort">Phone</th>
                          <th class="no-sort hidden"></th>
                          <th class="no-sort">Action</th>
                        </tr>
                        </thead>
                        <tbody id="sistersConcernDataTable">
				
                        </tbody>
                      </table>
                </div>
              </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
        <script src="../../Admin/jquery-3.6.0.min.js"></script>
    <script src="../../Scripts/Admin/SistersConcern.js"></script>
</asp:Content>

