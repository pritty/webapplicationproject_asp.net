﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class ChatMessagesUI : Page
    {
        public static BLL.Admin.ChatMessageManager AChatMessageManager = new BLL.Admin.ChatMessageManager();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static int SaveChatMessage(ChatMesageBox chatObj)
        {
            return AChatMessageManager.SaveChatMessage(chatObj);
        } 
        
        [WebMethod]
        public static List<ChatMesageBox> GetAllItemList()
        {
            return AChatMessageManager.GetAllItemList();
        }
    }
}