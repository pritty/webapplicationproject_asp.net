﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class ByProductsUI : System.Web.UI.Page
    {
        public static ByProductManager AByProductManager = new ByProductManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string DeleteAProduct(int Id, string imgUrl)
        {
            return AByProductManager.DeleteAProduct(Id, imgUrl); 
        }
    }
}