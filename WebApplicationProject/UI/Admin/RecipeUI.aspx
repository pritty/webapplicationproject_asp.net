﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="RecipeUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.RecipeUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	    background: #00c0ef;
    }
        .clearSpace{
            margin-top:6px;
        }
                .tableHeaderColor {
            background: #00c0ef;
        }
        .dataTables_wrapper {
	        padding: 5px;
	        margin: 5px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recipe Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="title">Title:</label>
                        <input type="hidden"  name="varId" id="varId" value="" />
                        <input type="text" class="form-control" name="title" id="title" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="subTitle">Sub Title:</label>
                        <input type="text" class="form-control" name="subTitle" id="subTitle" placeholder="Sub Title" value="" />
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="madeByRecipie">Made By Recipie:</label>
                        <input type="text" class="form-control" name="madeByRecipie" id="madeByRecipie" placeholder="Made By Recipie" value="" />
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="videoUrl">VideoUrl:</label>
                        <input type="text" class="form-control" name="videoUrl" id="videoUrl" placeholder="Video Url" value="" />
                    </div> 
             </div>
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="recipeImage">Image:</label>
                        <input type="file" class="form-control" name="recipeImage" id="recipeImage" value="" />
                        <input type="hidden"  id="imgUrl" value=""/>
                        <input type="hidden"  id="imgName" value=""/>
                    </div>
                   <div class="form-group">
                       <label class="control-label" for="">Preview Image:</label>
                         <img class="img-responsive" src=" " style="height: 250px; width: 300px" id="prevImg"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="shortDescription">Short Description:</label>
                        <textarea class="form-control" id="shortDescription" name="shortDescription" placeholder="Short Description" rows="4" cols="80">
                        </textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="detailsDescription">Details Description:</label>
                        <textarea class="form-control" id="detailsDescription" name="detailsDescription" placeholder="Details Description" rows="4" cols="80">
                        </textarea>
                    </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn" onclick="SaveRecipe();">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
              
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="gridViewTable" class="table table-bordered table-hover dataTable">
                                <thead class="" style="background:#00c0ef">
                                <tr role="row" class="">
                                    <th>SL NO</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Made By</th>
                                    <th>Short Description</th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="recipeTableBody">                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
        <script src="../../Admin/jquery-3.6.0.min.js"></script>
    <!-- CK Editor -->
<script src="../../Admin/bower_components/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        CKEDITOR.replace('shortDescription');
        CKEDITOR.replace('detailsDescription');
    })
</script>

    <script src="../../Scripts/Admin/Recipe.js"></script>
</asp:Content>

