﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ContactUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.ContactUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	    background: #00c0ef;
    }
        .clearSpace{
            margin-top:6px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="heading">Heading:</label>
                        <input type="hidden"  name="contactId" id="contactId" value="" />
                        <input type="text" class="form-control" name="heading" id="heading" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="phon">Phon:</label>
                        <input type="text" class="form-control" name="phon" id="phon" placeholder="Phon" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="phon2">Phon2:</label>
                        <input type="text" class="form-control" name="phon2" id="phon2" placeholder="Phon2" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="mobile">Mobile:</label>
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="" />
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="mobile2">Mobile2:</label>
                        <input type="text" class="form-control" name="mobile2" id="mobile2" placeholder="Mobile2" value="" />
                    </div>
             </div>
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="address">Address:</label>
                        <textarea class="form-control" id="address" name="address" placeholder="Address" rows="4" cols="80">
                        </textarea>
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="address2">Address2:</label>
                        <textarea class="form-control" id="address2" name="address2" placeholder="Address2" rows="4" cols="80">
                        </textarea>
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="mapUrl">Map URL:</label>
                        <input type="text" class="form-control" name="mapUrl" id="mapUrl" placeholder="Map URL" value="" />
                    </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
<div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table id="gridViewTable" class="table table-bordered table-striped dataTable" role="grid" >
                <thead class="" style="background:#00c0ef">
                <tr role="row" class="">
                    <th>SL NO</th>
                    <th>Heading</th>
                    <th>Address</th>
                    <th>Address2</th>
                    <th>Phone</th>
                    <th>Phone2</th>
                    <th>Mobile</th>
                    <th>Mobile2</th>
                    <th>MapUrl</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="contactTableBody">                   
                </tbody>
              </table>

        </div>

    </div>

  </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
    <script src="../../Admin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/Admin/Contact.js"></script>
</asp:Content>
