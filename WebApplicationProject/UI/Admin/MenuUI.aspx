﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MenuUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.MenuUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	        background: #00c0ef;
        }
        .clearSpace{
            margin-top:6px;
        }
        .tableHeaderColor {
            background: #00c0ef;
        }
        .dataTables_wrapper {
	        padding: 5px;
	        margin: 5px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
   <!-- message box -->
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
           <!--End message box -->
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Menu Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="menuTitle">Menu Title:</label>
                        <input type="hidden"  name="varId" id="varId" value="" />
                        <input type="text" class="form-control" name="menuTitle" id="menuTitle" placeholder="Menu Title" value="" />
                    </div> 
             </div>
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="menuUrl">MenuURL:</label>
                        <input type="text" class="form-control" name="menuUrl" id="menuUrl" placeholder="Menu URL" value="" />
                    </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
<div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table id="gridViewTable" class="table table-bordered table-striped dataTable" role="grid" >
                <thead class="" style="background:#00c0ef">
                <tr role="row" class="">
                    <th>SL NO</th>
                    <th>Menu Title</th>
                    <th>Menu Url</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="menuTableBody">                   
                </tbody>
              </table>

        </div>

    </div>

  </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
    <script src="../../Admin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/Admin/Menu.js"></script>
</asp:Content>
