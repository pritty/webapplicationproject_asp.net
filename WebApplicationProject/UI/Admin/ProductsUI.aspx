﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ProductsUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.ProductsUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	    background: #00c0ef;
    }
        .clearSpace{
            margin-top:6px;
        }
       .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Products Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="productTitle">Product Title:</label>
                        <input type="hidden"  name="productId" id="productId" value="" />
                        <input type="text" class="form-control" name="productTitle" id="productTitle" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="imageHeading">Image Heading:</label>
                        <input type="text" class="form-control" name="imageHeading" id="imageHeading" placeholder="Image Heading" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="description">Description:</label>
                        <textarea class="form-control" id="description" name="description" placeholder="Description" rows="4" cols="80">
                        </textarea>
                    </div>
             </div>
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="productImage">Image:</label>
                        <input type="file" class="form-control" name="productImage" id="productImage" placeholder="productImage" value="" />
                        <input type="hidden"  id="imgUrl" value=""/>
                        <input type="hidden"  id="imgName" value=""/>
<%--                        <input type="file" name="productImage" class="form-control" id="productImage" accept="image/*" onchange="loadPrevImgFile(event)" height="200" max-width="150"/>--%>
                    </div>
                   <div class="form-group">
                       <label class="control-label" for="">Preview Image:</label>
                         <img class="img-responsive"  style="height: 250px; width: 300px" id="prevImg"/>
                    </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn" onclick="SaveProduct();">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
<div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table id="gridViewTable" class="table table-bordered table-striped dataTable" role="grid" >
                <thead class="" style="background:#00c0ef">
                <tr role="row" class="">
                    <th>SL NO</th>
                    <th>Title</th>
                    <th>Image Heading</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="productTableBody">                   
                </tbody>
              </table>

        </div>

    </div>

  </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
        <script src="../../Admin/jquery-3.6.0.min.js"></script>
<%--    <script src="../../Admin/bower_components/jquery/dist/jquery.min.js"></script>--%>
<%--    <script>
        var loadPrevImgFile = function (event) {
            var output = document.getElementById('prevImg');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
</script>--%>
    <script src="../../Scripts/Admin/Product.js"></script>
</asp:Content>

