﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ChatMessagesUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.ChatMessagesUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <!-- bootstrap wysihtml5 - text editor -->
    <style>
        .box-header {
	    background: #00c0ef;
    }
        .clearSpace{
            margin-top:6px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="" id="message_box" style="font-style: italic; color: red; text-align: center">
    </div>
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Messages Form </h3>
            </div>
            <!-- /.box-header -->

              <!-- /.box-body -->


<div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table id="gridViewTable" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead class="" style="background:#00c0ef">
                <tr role="row" class="">
                    <th>SL NO</th>
                    <th>QuestionCategory</th>
                    <th>Question</th>
                    <th>PersonName</th>
                    <th>ContactType</th>
                    <th>Email</th>
                    <th>ContactNo</th>
                    <th class="hidden"></th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="messageTableBody">                   
                </tbody>
              </table>

        </div>

    </div>

  </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
        <script src="../../Admin/jquery-3.6.0.min.js"></script>
    <script src="../../Admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- CK Editor -->
<script src="../../Admin/bower_components/ckeditor/ckeditor.js"></script>
 <!-- CK Editor -->
<script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            CKEDITOR.replace('companyDescription');
            CKEDITOR.replace('mission');
            CKEDITOR.replace('vission');
        })
</script>

    <script src="../../Scripts/Admin/ChatMessage.js"></script>
</asp:Content>
