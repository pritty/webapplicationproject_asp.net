﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class LoginUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //    DateTime d;
            if (HttpContext.Current.Session["operator"] != null)
            {
                Response.Redirect("~/Admin.aspx");
            }
        }

        [WebMethod]
        public static tblOperator CheckUserLogin(string userId, string pass)
        {
            tblOperator atblOperator = new tblOperator();
            atblOperator.UserId = userId;
            atblOperator.Password = pass;
            OperatorManager operatorManager = new OperatorManager();
            tblOperator loggedtblOperator = operatorManager.Login(atblOperator);

            if (atblOperator.OperatorId > 0)
            {

                HttpContext.Current.Session.Add("operator", atblOperator);
            }

            return loggedtblOperator;
        }

        [WebMethod]
        public static string ChangetblOperatorPass(string pass)
        {
            tblOperator atblOperator = (tblOperator)HttpContext.Current.Session["operator"];

            atblOperator.Password = pass;

            OperatorManager operatorManager = new OperatorManager();
            int value = operatorManager.UpdatetblOperator(atblOperator); // return number based on input status

            if (value == -1)
            {
                return "Pass change failed";
            }
            else if (value > 0)
            {
                return "Password Changed Successfully";
            }
            else
            {
                return "Password change failed";
            }
        }
    }
}