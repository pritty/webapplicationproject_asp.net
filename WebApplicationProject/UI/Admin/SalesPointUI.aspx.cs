﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class SalesPointUI : System.Web.UI.Page
    {
        public static SalesPointManager ASalesPointManager = new SalesPointManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string SaveSalesPoint(SalesPoint salesPointObj)
        {
            return ASalesPointManager.SaveSalesPoint(salesPointObj);
        } 
        
        [WebMethod]
        public static List<SalesPoint> GetAllSalesPointList()
        {
            return ASalesPointManager.GetAllSalesPointList();
        }
        [WebMethod]
        public static string DeleteSalesPoint(int Id)
        {
            return ASalesPointManager.DeleteSalesPoint(Id);
        }
    }
}