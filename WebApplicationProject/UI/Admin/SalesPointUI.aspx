﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SalesPointUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.SalesPointUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	        background: #00c0ef;
        }
        .clearSpace{
            margin-top:6px;
        }
        .tableHeaderColor {
            background: #00c0ef;
        }
        .dataTables_wrapper {
	        padding: 5px;
	        margin: 5px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
            <!-- Message Box -->
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
        <!-- End Message Box -->
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">SalesPoint Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="row">
                    <div class="form-group col-md-2">
                    <label class="control-label" for="branchId">Sales Pont Id:</label>
                        <input type="hidden"  name="varId" id="varId" value="" />
                        <input type="text" class="form-control" name="branchId" id="branchId" value="" />
                    </div> 
                    <div class="form-group col-md-3">
                    <label class="control-label" for="salesPointName">Sales Point Name:</label>
                        <input type="text" class="form-control" name="salesPointName" id="salesPointName" placeholder="Sales Point Name" value="" />
                    </div> 
                    <div class="form-group col-md-3">
                    <label class="control-label" for="managerName">Manager Name:</label>
                        <input type="text" class="form-control" name="managerName" id="managerName" placeholder="Manager Name" value="" />
                    </div> 
                    <div class="form-group col-md-2">
                    <label class="control-label" for="mobile">Mobile:</label>
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="" />
                    </div>
                    <div class="form-group col-md-2">
                    <label class="control-label" for="phone">Phone:</label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="" />
                    </div>
             </div>
             <div class="row">
                    <div class="form-group col-md-3">
                    <label class="control-label" for="email">Email:</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="" />
                    </div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="webAddress">Web Address:</label>
                        <input type="text" class="form-control" name="webAddress" id="webAddress" placeholder="Web Address" value="" />
                    </div>
                    <div class="form-group col-md-5">
                    <label class="control-label" for="address">Address:</label>
                        <textarea class="form-control" id="address" name="address" placeholder="Address" rows="4" cols="50">
                        </textarea>
                    </div>
                    <div class="form-group">
                    <label class="control-label" for="mapUrl">Map URL:</label>
                        <input type="text" class="form-control" name="mapUrl" id="mapUrl" placeholder="Map URL" value="" />
                    </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />
<div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table id="gridViewTable" class="table table-bordered table-striped dataTable" role="grid" >
                <thead class="" style="background:#00c0ef">
                <tr role="row" class="">
                    <th>SL NO</th>
                    <th>Sales Point Id</th>
                    <th>Sales Point Name</th>
                    <th>Manage rName</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Phone</th>
                    <th>WebAddress</th>
                    <th>Email</th>
                    <th>MapUrl</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="salesPointTableBody">                   
                </tbody>
              </table>

        </div>

    </div>

  </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
    <script src="../../Admin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/Admin/SalesPoint.js"></script>
</asp:Content>
