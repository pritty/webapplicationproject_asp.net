﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VideoGalleryUI.aspx.cs" Inherits="WebApplicationProject.UI.Admin.VideoGalleryUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .box-header {
	        background: #00c0ef;
        }
        .clearSpace{
            margin-top:6px;
        }
        .tableHeaderColor {
            background: #00c0ef;
        }
        .dataTables_wrapper {
	        padding: 5px;
	        margin: 5px;
        }
        .alert-info {
            background: rgb(10, 184, 104);
            color: #fff;
        }
        .alert-danger{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .error{
            background: rgb(211, 17, 17);
            color: #fff;
        }
        .alert-warning {
            background: rgb(235, 156, 12);
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="alert" id="message_box" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
    <div class="" id="msgBox2" style="text-align: center">
    </div>
      <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Form </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
             <div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="title">Title:</label>
                        <input type="hidden"  name="videoId" id="videoId" value="" />
                        <input type="text" class="form-control" name="title" id="title" value="" />
                    </div> 
                    <div class="form-group">
                    <label class="control-label" for="videoUrl">Video URL:</label>
                        <input type="text" class="form-control" name="videoUrl" id="videoUrl" placeholder="iFrame" value="" />
                    </div>

             </div>
             <div class="col-md-6">
                <div class="form-group">
                <label class="control-label" for="description">Description:</label>
                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="10" cols="80">
                    </textarea>
                </div>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" name="saveBtn" id="saveBtn">Save</button>
                <button class="btn btn-default" type="button" name="clearBtn" id="clearBtn">Cancel</button>
                <button type="button" class="btn btn-danger" name="deleteBtn" id="deleteBtn">Delete</button>
              </div>
              <div class="clearSpace"></div>
              <hr />

                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="gridViewTable" class="table table-bordered table-hover dataTable">
                                <thead class="tableHeaderColor">
                                <tr role="row">
                                    <th class="" colspan=" ">Sl No</th>
                                    <th class="" rowspan=" " colspan=" "  >Title</th>
                                    <th class="" rowspan=" " colspan=" "  >Description</th>
                                    <th class="" rowspan=" " colspan=" "  >Image</th>
                                    <th class="hidden"></th>
                                    <th class="" rowspan=" " colspan=" "  >Action</th>
                                </tr>
                                </thead>
                                <tbody id="videoGalleryTable">

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
          </div>
          <!-- /.box -->
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
    <script src="../../Admin/jquery-3.6.0.min.js"></script>
<!-- CK Editor -->
<script src="../../Admin/bower_components/ckeditor/ckeditor.js"></script>
<script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            CKEDITOR.replace('description');
        })
</script>
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

</script>
    <script src="../../Scripts/Admin/VideoGallery.js"></script>
</asp:Content>
