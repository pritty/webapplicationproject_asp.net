﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class ContactUI : System.Web.UI.Page
    {
        public static ContactManager AContactManager=new ContactManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string SaveContact(contact contactObj)
        {
            return AContactManager.SaveContact(contactObj);
        } 
        
        [WebMethod]
        public static List<contact> GetAllContactItemList()
        {
            return AContactManager.GetAllContactItemList();
        }
        [WebMethod]
        public static string DeleteContact(int Id)
        {
            return AContactManager.DeleteContact(Id);
        }
    }
}