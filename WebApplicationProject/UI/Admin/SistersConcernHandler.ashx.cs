﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.UI.Admin
{
    /// <summary>
    /// Summary description for SistersConcernHandler
    /// </summary>
    public class SistersConcernHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string outputToReturn = String.Empty;
            string base64string = String.Empty;
            MemoryStream ms = new MemoryStream();
            int count = 0;
            SisterrsConcern aSisterrsConcern = new SisterrsConcern();
            context.Response.ContentType = "text/json";
            string num = DateTime.Now.Ticks.ToString();
            var ac = string.Empty;
            var Id = 0;
            var ProductTitle = String.Empty;
            var ProductImageHeading = String.Empty;
            var ProductImage = String.Empty;
            var Description = String.Empty;
            var ProductImageName = String.Empty;
            var ProductImageUrl = String.Empty;
            var ProductImageBinary = String.Empty;
            //string imgUrl = context.Request["imgUrl"] ?? "";  
            string DataItemString = "";
            byte[] imageBytes;

            List<product> statusesList;
            string path = "";
            string fileName = "";

            // This will avoid errors     
            if (!string.IsNullOrEmpty(context.Request["ac"]))
            {
                ac = context.Request["ac"];

                if (ac.Equals("send")) // Use Equals instead of just =  as it also compares objects     
                {
                    context.Response.ContentType = "application/json";

                    int attId = Convert.ToInt32(context.Request.QueryString["sisterConcernObj"]);
                    var requestdata = context.Request;
                    var sr = new StreamReader(requestdata.InputStream);
                    var stream = sr.ReadToEnd();
                    var javaScriptSerializer = new JavaScriptSerializer();
                    //string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

                    //deserialize the object
                    NewSistersConcern data = Deserialize<NewSistersConcern>(stream);

                    if (data != null)
                    {
                        //string baseStringImage = data.sisterConcernObj.HedarImg;
                        //string[] tokens = baseStringImage.Split(';');
                        //base64string = tokens[1].Replace("base64,", "");
                        //string[] imageName = tokens[0].Split('/');
                        //string extention = imageName[1];
                        //imageBytes = Convert.FromBase64String(base64string);
                        //ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                        //fileName = num;
                        //string imagePath = SaveImageToFile(base64string, fileName, extention);

                        Id = data.sisterConcernObj.Id;
                        //ProductTitle = data.sisterConcernObj.ProductTitle;
                        //ProductImageHeading = data.sisterConcernObj.ProductImageHeading;
                        //ProductImage = base64string;
                        //Description = data.sisterConcernObj.Description;
                        //ProductImageName = fileName + "." + extention;
                        //ProductImageUrl = imagePath;
                        //StreamReader readerImageBinary = new StreamReader(ms);
                        //ProductImageBinary = readerImageBinary.ReadToEnd();
                        imageBytes = ms.ToArray();
                        byte[] bytes = new byte[ms.Length];
                        new System.Data.Linq.Binary(bytes);
                        var byteArray = Encoding.ASCII.GetBytes(ms.ToString());

                        aSisterrsConcern.Id = Id;
                        aSisterrsConcern.CompanyName = data.sisterConcernObj.CompanyName;
                        aSisterrsConcern.Address = data.sisterConcernObj.Address;
                        aSisterrsConcern.Email = data.sisterConcernObj.Email;
                        aSisterrsConcern.MobileNo1 = data.sisterConcernObj.MobileNo1;
                        aSisterrsConcern.MobileNo2 = data.sisterConcernObj.MobileNo2;
                        aSisterrsConcern.MobileNo3 = data.sisterConcernObj.MobileNo3;

                    }
                    else
                    {
                        context.Response.Write("No Data");
                    }

                    SistersConcernGateway aSistersConcernGateway = new SistersConcernGateway();
                    if (Id > 0)
                    {
                        count = aSistersConcernGateway.UpdateSisterrsConcern(aSisterrsConcern);
                        //if (count > 0)
                        //{
                        //    if (System.IO.File.Exists(data.sisterConcernObj.ProductImageUrl))
                        //        System.IO.File.Delete(data.sisterConcernObj.ProductImageUrl);
                        //}
                        //else
                        //{
                        //    if (System.IO.File.Exists(aSisterrsConcern.ProductImageUrl))
                        //        System.IO.File.Delete(aSisterrsConcern.ProductImageUrl);
                        //}
                    }
                    else
                    {
                        count = aSistersConcernGateway.SaveSisterrsConcern(aSisterrsConcern);
                        //if (count <= 0)
                        //{
                        //    if (System.IO.File.Exists(aSisterrsConcern.ProductImageUrl))
                        //        System.IO.File.Delete(aSisterrsConcern.ProductImageUrl);
                        //}
                    }
                }

                // You need to Send your object as a JSON Object         
                // You are just sending a sting  

                context.Response.ContentType = "text/json";
                context.Response.Write(count);
                //context.Response.End();
            }


            else if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    fname = Path.Combine(context.Server.MapPath("~/uploads/"), fname);
                    file.SaveAs(fname);
                }
            }


            else
            {
                //Fetch all Files from Database Table.
                List<SisterrsConcern> aList = new List<SisterrsConcern>();

                SistersConcernGateway aSistersConcernGateway = new SistersConcernGateway();
                aList = aSistersConcernGateway.GetAllSistersConcernList();

                //Send File data in JSON format for Download.
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = Int32.MaxValue;
                //string json = new JavaScriptSerializer().Serialize(files);
                string json = serializer.Serialize(aList);
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.ContentType = "text/json";
                context.Response.Write(json);
                context.Response.End();
            }

        }

        //[Serializable]
        //public class ListOfTableImage
        //{
        //    [JsonProperty("ListOfTableImage")]
        //    public List<Models.product> ListOfTableImages { get; set; }
        //}

        //[Serializable]
        //public class ListOfTableImage2
        //{
        //    public product ListOfTableImages { get; set; }
        //} 

        [Serializable]
        public class NewSistersConcern
        {
            [JsonProperty("NewSistersConcern")]
            public SisterrsConcern sisterConcernObj { get; set; }
        }

        public T Deserialize<T>(string context)
        {
            string jsonData = context;
            //cast to specified objectType
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public string SaveImageToFile(string ImgStr, string ImgName, string extention)
        {
            String path = HttpContext.Current.Server.MapPath("~/uploads/SistersConcern"); //Path

            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = ImgName + "." + extention;

            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(ImgStr);

            File.WriteAllBytes(imgPath, imageBytes);

            return imgPath;
            //return true;
        }

        public Stream ConvertToBase64(Stream stream)
        {
            Byte[] inArray = new Byte[(int)stream.Length];
            Char[] outArray = new Char[(int)(stream.Length * 1.34)];
            stream.Read(inArray, 0, (int)stream.Length);
            Convert.ToBase64CharArray(inArray, 0, inArray.Length, outArray, 0);
            return new MemoryStream(Encoding.UTF8.GetBytes(outArray));
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}