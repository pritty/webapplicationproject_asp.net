﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject.UI.Admin
{
    public partial class About : System.Web.UI.Page
    {
        public static AboutManager aAboutManager = new AboutManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string SaveAbout(about aboutObj)
        {
            return aAboutManager.SaveAbout(aboutObj);
        }
        
        [WebMethod]
        public static List<about> GetAllItemList()
        {
            return aAboutManager.GetAllAboutList();
        } 
        [WebMethod]
        public static string DeleteAbout(int Id)
        {
            return aAboutManager.DeleteAbout(Id);
        }
    }
}