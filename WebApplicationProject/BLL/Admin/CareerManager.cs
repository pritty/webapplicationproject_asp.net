﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class CareerManager
    {
        public CareerGateway ACareerGateway = new CareerGateway();
        public string SaveCareer(Career careerObj)
        {
            int x = 0;
            if (careerObj.Id > 0)
            {
                x = ACareerGateway.UpdateCareer(careerObj);
                if (x>0)

                    return "Updated";
                return "Failed";
            }
            else
            {
                x = ACareerGateway.SaveCareer(careerObj);
                if (x > 0)
                    return "Saved";
                return "Failed";
            }                      
        }

        public List<Career> GetAllCareerList()
        {
            return ACareerGateway.GetAllCareerList();
        }
        public string DeleteCareer(int aboutId)
        {
            int x = ACareerGateway.DeleteCareer(aboutId);
            if (x > 0)
                return "Deleted";
            return "Failed";
        }
    }
}