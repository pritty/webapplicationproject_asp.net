﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class SliderManager
    {
        public SliderGateway ASliderGateway = new SliderGateway();
        public List<slider> GetAllSlider()
        {
            return ASliderGateway.GetAllSliderList();
        }
        public string DeleteASlider(int Id, string imgUrl)
        {
            int x = ASliderGateway.DeleteASlider(Id);
            if (x > 0)
            {
                if (System.IO.File.Exists(imgUrl))
                    System.IO.File.Delete(imgUrl);
                return "Deleted";
            }
            else
            {
                return "Failed";
            }
        }
    }
}