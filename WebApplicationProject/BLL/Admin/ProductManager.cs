﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class ProductManager
    {
        public ProductGateway  AProductGateway=new ProductGateway();
        public string DeleteAProduct(int Id, string imgUrl)
        {
            int x= AProductGateway.DeleteAProduct(Id);
            if (x > 0)
            {
                if (System.IO.File.Exists(imgUrl))
                    System.IO.File.Delete(imgUrl);
                return "Deleted";
            }
            else
            {
                return "Failed";
            }
        }

        public List<product> GetAllProduct()
        {
            return AProductGateway.GetAllProductList();
        }
    }
}