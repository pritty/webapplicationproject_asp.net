﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class LogoManager
    {
        public LogoGateway ALogoGateway = new LogoGateway();
        public string DeleteALogo(int Id, string imgUrl)
        {
            int x = ALogoGateway.DeleteALogo(Id);
            if (x > 0)
            {
                if (System.IO.File.Exists(imgUrl))
                    System.IO.File.Delete(imgUrl);
                return "Deleted";
            }
            else
            {
                return "Failed";
            }
        }
    }
}