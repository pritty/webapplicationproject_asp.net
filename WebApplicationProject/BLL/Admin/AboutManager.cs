﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class AboutManager
    {
        public AboutGateway aAboutGateway = new AboutGateway();
        public string SaveAbout(about aboutObj)
        {
            int x = 0;
            if (aboutObj.Id > 0)
            {
                 x = aAboutGateway.UpdateAbout(aboutObj);
                if (x>0)

                    return "Updated";
                return "Failed";
            }
            else
            {
                x = aAboutGateway.SaveAbout(aboutObj);
                if (x > 0)
                    return "Saved";
                return "Failed";
            }                      
        }

        public List<about> GetAllAboutList()
        {
            return aAboutGateway.GetAllAboutList();
        }
        public string DeleteAbout(int aboutId)
        {
            int x=aAboutGateway.DeleteAbout(aboutId);
            if (x > 0)
                return "Deleted";
            return "Failed";
        }
    }
}