﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class ContactManager
    {
        public ContactGateway AContactGateway=new ContactGateway();
        public string SaveContact(contact contactObj)
        {
            int x = 0;
            if (contactObj.Id> 0)
            {
                x = AContactGateway.UpdateContact(contactObj);
                if (x > 0)

                    return "Updated";
                return "Failed";
            }
            else
            {
                x = AContactGateway.SaveContact(contactObj);
                if (x > 0)
                    return "Saved";
                return "Failed";
            } 
        }
        public List<contact> GetAllContactItemList()
        {
            return AContactGateway.GetAllContactItemList();
        }
        public string DeleteContact(int Id)
        {
            int x= AContactGateway.DeleteContact(Id);
            if(x>0)
                return "Deleted";
            return "Failed";
        }
    }
}