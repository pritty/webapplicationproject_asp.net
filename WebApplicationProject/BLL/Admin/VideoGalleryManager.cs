﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class VideoGalleryManager
    {
        public VideoGalleryGateway AVideoGalleryGateway = new VideoGalleryGateway();
        public string SaveVideoGallery(VideoGallery galleryObj)
        {
            int x = 0;
            if (galleryObj.Id > 0)
            {
                x = AVideoGalleryGateway.UpdateGallery(galleryObj);
                if (x > 0)

                    return "Updated";
                return "Failed";
            }
            else
            {
                x = AVideoGalleryGateway.SaveVideoGallery(galleryObj);
                if (x > 0)
                    return "Saved";
                return "Failed";
            } 
        }
        public List<VideoGallery> GetAllGalleryList()
        {
            return AVideoGalleryGateway.GetAllGalleryList();
        }
        public string DeleteGallery(int Id)
        {
            int x = AVideoGalleryGateway.DeleteGallery(Id);
            if(x>0)
                return "Deleted";
            return "Failed";
        }
    }
}