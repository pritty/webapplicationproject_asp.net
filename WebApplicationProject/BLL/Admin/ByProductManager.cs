﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class ByProductManager
    {
        public ByProductGateway AByProductGateway = new ByProductGateway();
        public string DeleteAProduct(int Id, string imgUrl)
        {
            int x = AByProductGateway.DeleteAProduct(Id);
            if (x > 0)
            {
                if (System.IO.File.Exists(imgUrl))
                    System.IO.File.Delete(imgUrl);
                return "Deleted";
            }
            else
            {
                return "Failed";
            }
        }

        public List<ByProduct> GetAllProduct()
        {
            return AByProductGateway.GetAllProductList();
        }
    }
}