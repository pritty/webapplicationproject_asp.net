﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class ChatMessageManager
    {
        public  ChatMessageGateway AChatMessageGateway=new ChatMessageGateway();
        public int SaveChatMessage(ChatMesageBox chatObj)
        {
            return AChatMessageGateway.SaveChatMessage(chatObj);
        }

        public List<ChatMesageBox> GetAllItemList()
        {
            return AChatMessageGateway.GetAllChatbotMessageList();
        }
    }
}