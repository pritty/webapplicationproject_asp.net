﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class SistersConcernManager
    {
        public SistersConcernGateway ASistersConcernGateway = new SistersConcernGateway();
        public string DeleteASistersConcern(int Id)
        {
            int x = ASistersConcernGateway.DeleteASistersConcern(Id);
            if (x > 0)
            {
                return "Deleted";
            }
            else
            {
                return "Failed";
            }
        }
    }
}