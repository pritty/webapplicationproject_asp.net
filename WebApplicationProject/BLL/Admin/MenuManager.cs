﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class MenuManager
    {
        public MenuGateway AMenuGateway = new MenuGateway();
        public string SaveMenu(menu menuObj)
        {
            int x = 0;
            if (menuObj.Id > 0)
            {
                x = AMenuGateway.UpdateMenu(menuObj);
                if (x > 0)

                    return "Updated";
                return "Failed";
            }
            else
            {
                x = AMenuGateway.SaveMenu(menuObj);
                if (x > 0)
                    return "Saved";
                return "Failed";
            } 
        }
        public List<menu> GetAllMenuItemList()
        {
            return AMenuGateway.GetAllMenuItemList();
        }
        public string DeleteAMenu(int Id)
        {
            int x = AMenuGateway.DeleteAMenu(Id);
            if(x>0)
                return "Deleted";
            return "Failed";
        }
    }
}