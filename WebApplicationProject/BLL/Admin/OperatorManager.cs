﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class OperatorManager
    {
        public tblOperator Login(tblOperator atblOperator)
        {
            OperatorGateway operatorGateway = new OperatorGateway();
            return operatorGateway.Login(atblOperator);
        }
        public bool CheckPageAccess(int oId, string pageName)
        {
            OperatorGateway operatorGateway = new OperatorGateway();
            return operatorGateway.CheckPageAccess(oId, pageName);
        }
        public bool CheckLogIn(tblOperator atblOperator)
        {
            OperatorGateway operatorGateway = new OperatorGateway();
            return operatorGateway.CheckLogIn(atblOperator);
        }
        public bool LogOut(tblOperator atblOperator)
        {
            OperatorGateway operatorGateway = new OperatorGateway();
            return operatorGateway.LogOut(atblOperator);
        }

        public int UpdatetblOperator(tblOperator atblOperator)
        {
            OperatorGateway operatorGateway = new OperatorGateway();
            return operatorGateway.UpdatetblOperator(atblOperator);
        }
    }
}