﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class RecipeManager
    {
        public RecipeGateway ARecipeGateway = new RecipeGateway();
        public string DeleteARecipe(int Id, string imgUrl)
        {
            int x = ARecipeGateway.DeleteARecipe(Id);
            if (x > 0)
            {
                if (System.IO.File.Exists(imgUrl))
                    System.IO.File.Delete(imgUrl);
                return "Deleted";
            }
            else
            {
                return "Failed";
            }
        }
    }
}