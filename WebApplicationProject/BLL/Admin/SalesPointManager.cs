﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationProject.DAL.Admin;

namespace WebApplicationProject.BLL.Admin
{
    public class SalesPointManager
    {
        public SalesPointGateway ASalesPointGateway = new SalesPointGateway();
        public string SaveSalesPoint(SalesPoint salesPointObj)
        {
            int x = 0;
            if (salesPointObj.Id > 0)
            {
                x = ASalesPointGateway.UpdateSalesPoint(salesPointObj);
                if (x > 0)

                    return "Updated";
                return "Failed";
            }
            else
            {
                x = ASalesPointGateway.SaveSalesPoint(salesPointObj);
                if (x > 0)
                    return "Saved";
                return "Failed";
            } 
        }
        public List<SalesPoint> GetAllSalesPointList()
        {
            return ASalesPointGateway.GetAllSalesPointList();
        }
        public string DeleteSalesPoint(int Id)
        {
            int x = ASalesPointGateway.DeleteSalesPoint(Id);
            if(x>0)
                return "Deleted";
            return "Failed";
        }
    }
}