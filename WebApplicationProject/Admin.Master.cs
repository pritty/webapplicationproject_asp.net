﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationProject.BLL.Admin;

namespace WebApplicationProject
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        private string currentPage = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            tblOperator cO = new tblOperator();


            if (!IsPostBack)
            {
                cO = (tblOperator)Session["operator"];
                currentPage = Path.GetFileName(Request.PhysicalPath);
                if (cO != null)
                {
                    if (currentPage != "Admin.aspx")
                    {
                        if (!CheckPageAccess(Convert.ToInt32(cO.OperatorId), currentPage))
                        {
                            //Response.Redirect("~/UI/Default.aspx");
                            Response.Redirect("~/Admin.aspx");
                        }
                    }
                    OperatorManager operatorManager = new OperatorManager();
                    if (!operatorManager.CheckLogIn(cO))
                    {
                        Response.Redirect("~/UI/Admin/LoginUI.aspx");
                    }

                    userId.Text = cO.UserId;
                    password.Text = cO.Password;


                    //if (cO.Type == "Administrator")
                    //{
                    //    menuManager.LoadMenuTreeForAdministrator((int)cO.OperatorId);
                    //}


                    xx = "";
                }
                else
                {
                    Response.Redirect("~/UI/Admin/LoginUI.aspx");
                }

            }
        }
        private bool CheckPageAccess(int oId, string pageName)
        {
            OperatorManager operatorManager = new OperatorManager();
            return operatorManager.CheckPageAccess(oId, pageName);
        }
        private string xx = "";
        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {

        }
    }
}