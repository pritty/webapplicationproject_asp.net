﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Front.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationProject._Default" %>

<%--<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>Modify this template to jump-start your ASP.NET application.</h2>
            </hgroup>

        </div>
    </section>
</asp:Content>--%>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="FrontContentPlaceHolder">
<%--    <style>
        #sliderWrapper .item img{
    height: 445px !important;
    width: 900px!important;
}
    </style>--%>
<%-- <div id="google_translate_element"></div>--%>
 <div class="container">


    <div id="topSlider" class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: push; autoplay: true; autoplayInterval: 3500;" uk-max-height="300">

        <ul class="uk-slideshow-items" id="ukSlidelist">

            <li>
                <img src="" alt="" uk-cover>
                <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <h3 class="uk-margin-remove"></h3>
                </div>
            </li>


        </ul>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

    </div>

</div>
<div class="about" style="padding: 4%;
    background: #fcffea;">

    <div class="about_slider_header">
        <h2>
            Maintain Quality To Get Optimum Results
            <br/>
            <small>We Ensure Your Success</small>
        </h2>
    </div>

    <div class="container about_slider">
        <div class="row">
                <div class="col-sm-6 col-md-4" style="margin: 15px 0;">

<%--                     <a style="display: inline-block; background: #ededed; padding: 10px; border-radius: 10px; width: 100%;" class="text-center industrySingle" href="view/front/product.php?id=<?php echo $sId;?>&name=<?php echo $sName;?>#product_<?php echo $tempProduct['id'];?>">
                          <img style="height: 250px; width: auto;" src="view/admin/product/<?php echo $tempProduct['product_image_url'];?>" class="img img-responsive img-thumbnail center-block" alt="<?php echo $tempProduct['product_title'];?>" />
                            <h3 style="height: 90px; "><?php echo $tempProduct['product_title'];?></h3>
                     </a>--%>
                  </div>


        </div>
    </div>
</div>
<div class="about" style="padding: 4%;
    background: #ededed;">

            <div class="container-fluid">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="container-fluid">
                            
                            <div class="about_slider_header">
                                <h2>
                                    About Us
                                    <br/>
                                    <small>Who We Are</small>
                                </h2>
                            </div>
                            
                                <div class="sectionDes" id="companyDesShort">

                                </div>
                            
                        </div>
                    </div>

                </div>
            </div>
    </div>
  <div class="about" style="padding: 4%;
    background: #d6ffc9;">

        <div class="container-fluid">

            <div class="about_slider_header">
                <h2>
                    Our Product 
                </h2>
            </div>
            <br/><br/>
            <div uk-slider>

                <div class="uk-position-relative logo_slider">

                    <div class="uk-slider-container uk-light">
                        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m" id="someProductSlide">

                            <li>
<%--                                <a target="_blank" href="<?php echo $partner['client_website_link'];?>">
                                    <img class="center-block" src=" " alt="">
                                </a>--%>
                            </li>

                        </ul>
                    </div>

                    <div class="uk-hidden@s uk-light">
                        <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    </div>

                    <div class="uk-visible@s">
                        <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    </div>

                </div>

                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

            </div>


        </div>
    </div>
  <div class="about" style="padding: 4%;
    background: #333;">

            <div class="container-fluid">
                <div class="row">

                    <div class="col-sm-12">

                            <div class="about_slider_header">
                                <h2>
                                    Our Sales Point
                                    <br/>
                                    <small>Who Made Us</small>
                                </h2>
                            </div>
                            
<br/><br/>
            <div uk-slider>

                <div class="uk-position-relative logo_slider">

                    <div class="uk-slider-container uk-light">
                        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-5@m" id="salesPoint">

<%--                            <li>
                                <a target="_blank" href="<?php echo $client['client_website_link'];?>">
                                    <img class="center-block" src="view/admin/my_connections/images/<?php echo $client['client_logo_name'];?>" alt="">
                                </a>
                            </li>--%>


                        </ul>
                    </div>

                    <div class="uk-hidden@s uk-light">
                        <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    </div>

                    <div class="uk-visible@s">
                        <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    </div>

                </div>

                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

            </div>

</div>

                </div>
            </div>
        </div>
    <script src="Scripts/Frontend/Default.js"></script>
</asp:Content>
