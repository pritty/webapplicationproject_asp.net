﻿$(document).ready(function () {

    GetAllProduct();
});

//Global Variable
var page = 1, pageLimit = 1, totalRecord = 0;
var totalPage = 1;
var dataOfShow;

var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var productGlobalId = 0;

//************************************************************
function GetAllProduct() {
    $.ajax({
        url: "/../UI/Admin/ByProductHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            dataOfShow = "";
            dataOfShow = data;
            (function (name) {
                var container = $('#' + name);
                var options = {
                    dataSource: dataOfShow,
                    pageSize: 15,
                    callback: function (response, pagination) {
                        window.console && console.log(response, pagination);

                        LoadProductServiceBox(response);
                        //var html = "";

                        //$.each(response, function (index, item) {

                        //    //html += "<p>" + item[0].Description + "</p>";
                        //    html += "<p>" + item.ProductTitle + "</p>";
                        //    html += "<p>" + item.Description + "</p>";
                        //});

                        ////dataHtml += '</ul>';

                        ////container.prev().html(dataHtml);
                        //$("#dataint").html(html);
                    }
                };

                //$.pagination(container, options);

                container.addHook('beforeInit', function () {
                    window.console && console.log('beforeInit...');
                });
                container.pagination(options);

                container.addHook('beforePageOnClick', function () {
                    window.console && console.log('beforePageOnClick...');
                    //return false
                });
            })('simplePagination');
            //LoadProductServiceBox(data);
           // LoadProductTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function short_description(data) {
    var maxLength = 120;
    var description = data;
    var shortDes = "";

    if ($.trim(description).length > maxLength) {
        var newStr = description.substring(0, maxLength);
        var removedStr = description.substring(maxLength, $.trim(description).length);
        shortDes = newStr + "...";
    }
    return shortDes;
}
function LoadProductServiceBox(data) {
    var serviceBox = "";
    $("#productDiv").empty();
    var html = "";
    if (data != "" || data != null) {
        $.each(data, function (key, value) {
            //html += "<div class='col-sm-6 col-md-4 serviceBox'>" +
            //    "<a  href='ByProduct_Details.aspx?id=" + value.Id + "' class='serviceTitle'>" +
            //    "<img src='../../uploads/ByProducts/" + value.ProductImageName + "' class='img img-responsive img-circle center-block' alt='" + value.ProductTitle + "' />" +
            //    "" + value.ProductTitle + "" +
            //    "</a></div>";

            var description = "", shortDes = "";
            description = value.Description;
            shortDes = short_description(description);
            html += "<div class='col-md-4' style='margin: 15px 0;'>" +
                    "<a class='ByProductSingle' href='/UI/Frontend/ByProduct_Details.aspx?id=" + value.Id + "&name=" + value.ProductTitle + "'>" +
                        "<div class='content_wrapper'>" +
                            "<div class='coupon'>" +
                                    "<div class='logo_header'>" +
                                    "<h3>" + value.ProductTitle + "</h3>" +
                                    "</div>" +
                                    "<img src='../../uploads/ByProducts/" + value.ProductImageName + "' alt='Avatar' style='width: 100%;'/>" +
                              "</div>" +
                            "<div class='text_wrapper'>" +
                                " <p>" + shortDes + "</p> " +
                            "</div> " +
                        "</div> " +
                    "</a> " +
                "</div>";
            autoRowId++;
        });
    }
    $("#productDiv").html(html);
}
function LoadProductTable(data) {
    var tableData = "";
    $("#productTableBody").empty();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='contactListRow'id=product_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Title'>" + value.ProductTitle + "</td>";
            tableData += "<td class='Heading'>" + value.ProductImageHeading + "</td>";
            tableData += "<td class='imageName hidden'>" + value.ProductImageName + "</td>";
            tableData += "<td class='description'>" + value.Description + "</td>";
            tableData += "<td class='imgUrl hidden'>" + value.ProductImageUrl + "</td>";
            tableData += "<td class='image'><img src='data:image/jpg;base64," + value.ProductImage + "' width=300 height=250/></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#productTableBody").append(tableData);
}


