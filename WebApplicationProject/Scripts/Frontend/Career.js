﻿$(document).ready(function () {
     base_url = window.location.origin;
     host = window.location.host;
     pathArray = window.location.pathname.split('/');
     pageName = window.location.pathname.split("Frontend/");

     GetAllCareerList();
});

var autoRowId = 1;
var itemGlobalId = 0;
var base_url = "";
var host = "";
var pathArray = "";
var pageName = "";
var dataOfShow;


var htmlTextArray;
function GetAllCareerList() {
    $.ajax({
        url: "Career.aspx/GetAllCareerList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            htmlTextArray = data.d;
            dataOfShow = "";
            dataOfShow = data.d;
            (function (name) {
                var container = $('#' + name);
                var options = {
                    dataSource: dataOfShow,
                    pageSize: 20,
                    callback: function (response, pagination) {
                        window.console && console.log(response, pagination);

                        LoadCareerContent(response);
                    }
                };

                //$.pagination(container, options);

                container.addHook('beforeInit', function () {
                    window.console && console.log('beforeInit...');
                });
                container.pagination(options);

                container.addHook('beforePageOnClick', function () {
                    window.console && console.log('beforePageOnClick...');
                    //return false
                });
            })('simplePagination');
            
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}


function LoadCareerContent(data) {
    var html = "";
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            html += "<div class='col-md-12 singleCareer'><p class='careerSectionHeading designation'>" +
                "" + value.Designation + "" +
                "</p>" +
                "<div class='vertical_line careerDes'><p class='careerEmail'>" +
                "" + value.Email + "" +
                "</p>" +
                "<a target='_blank' class='careerSeeMoreBtn' href='/UI/Frontend/CareerDetails.aspx?Id=" + value.Id + "'>See More</a></div></div>";
            autoRowId++;
        });
    }
    $("#career").empty();
    $("#career").html(html);
}
function LoadDataTable(data) {
    var tableData = "";
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='careerListRow'id=career_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Designation'>" + value.Designation + "</td>";
            tableData += "<td class='Requirement'>" + value.Requirement + "</td>";
            tableData += "<td class='Description'>" + value.Description + "</td>";
            tableData += "<td class='Address hidden'>" + value.Address + "</td>";
            tableData += "<td class='ContactPersonName hidden'>" + value.ContactPersonName + "</td>";
            tableData += "<td class='ContactPersonDetails hidden'>" + value.ContactPersonDetails + "</td>";
            tableData += "<td class='Mobile'>" + value.Mobile + "</td>";
            tableData += "<td class='Phone'>" + value.Phone + "</td>";
            tableData += "<td class='Email'>" + value.Email + "</td>";
            tableData += "<td class='ApplicationStartDate hidden'>" + value.ApplicationStartDate + "</td>";
            tableData += "<td class='ApplicationLastDate'>" + value.ApplicationLastDate + "</td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#aboutTableBody").empty();
    $("#aboutTableBody").append(tableData);
}













