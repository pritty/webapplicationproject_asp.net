﻿$(document).ready(function () {

     full_url = document.URL; // Get current url
     url_array = full_url.split('/') // Split the string into an array with / as separator
     last_segment = url_array[url_array.length - 1];
     lastparam = last_segment.split('.aspx?');
     param = lastparam[lastparam.length - 1].split('&');

     urlId = param[0].substring(param[0].lastIndexOf('=') + 1);
     urlName = param[1].substring(param[1].lastIndexOf('=') + 1);
     //urlName = full_url.substring(full_url.lastIndexOf('=') + 1);

     GetAProduct(urlId, urlName);

});

//Global Variable
var full_url = "";
var url_array = "";
var last_segment = "";
var lastparam = "";
var param = "";
var urlId = "";
var urlName = "";

var autoRowId = 1;
var productGlobalId = 0;

function GetAProduct(id,productName) {
    $.ajax({
        url: "ByProduct_Details.aspx/GetAProduct",
        type: "POST",
        data: JSON.stringify({
            id: id,
            productName: productName
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            productTitle(data.d);
            LoadProductServiceBox(data.d);
        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}
function short_description(data) {
    var maxLength = 120;
    var description = data;
    var shortDes = "";

    if ($.trim(description).length > maxLength) {
        var newStr = description.substring(0, maxLength);
        var removedStr = description.substring(maxLength, $.trim(description).length);
        shortDes =newStr + "...";
    }
    return shortDes;
}
function productTitle(data) {
    $("#productTitle").empty();
            $("#productTitle").append(data.ProductTitle);
}
function LoadProductServiceBox(data) {
    $("#productDescription").empty();
    $("#imgDiv").empty();
    var html = "";

    html += "<img src='../../uploads/ByProducts/" + data.ProductImageName + "' alt='Avatar' style='width:100%;'/>" +
        "<p style='text-align:center;padding:10px'>" + data.ProductImageHeading + "</p>";

            $("#productDescription").html(data.Description);
            $("#imgDiv").html(html);
}


