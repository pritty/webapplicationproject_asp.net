﻿$(document).ready(function () {

    GetAllSlider();
    //$("#saveBtn").click(SaveContact);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
    $("#sliderImage").change(function () {
        readURL(this);
        var selectedFile = $("#sliderImage")[0].files[0];
        selectedFile.convertToBase64(function (base64) {
            //alert(base64);
            baseStringImage = "";
            blobImage = "";
            blobImage = makeblob(base64);
            baseStringImage = base64;

        });
    });
});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var sliderGlobalId = 0;

//*************************Image Manage**************************************
// preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#prevImg").attr("src", "");
            $('#prevImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function makeblob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);
        return new Blob([raw], { type: contentType });
    }
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
File.prototype.convertToBase64 = function (callback) {
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };
    reader.readAsDataURL(this);
};
//************************************************************
function GetAllSlider() {
    $.ajax({
        url: "SliderHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            LoadSliderTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadSliderTable(data) {
    var tableData = "";
    $("#sliderTableBody").empty();
    $("#gridViewTable").DataTable().destroy();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='sliderListRow'id=slider_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Title'>" + value.SliderTitle + "</td>";
            tableData += "<td class='imageName hidden'>" + value.ImageName + "</td>";
            tableData += "<td class='imgUrl hidden'>" + value.ImageLargeUrl + "</td>";
            tableData += "<td class='image'><img src='/uploads/Slider/" + value.ImageName + "' width=300 height=250/></td>";
            //tableData += "<td class='image'><img src='data:image/jpg;base64," + value.ImageLarge + "' width=300 height=250/></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#sliderTableBody").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}


// SAVE PRODUCT
function showMessage() {
    $("#message_box").html("All Items Required").addClass("alert alert-danger alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
}
function SaveSlider() {
    $("#saveBtn").prop("disabled", true);

    if ($("#saveBtn").text() == "Update") {
        UpdateSlider();
    }
    else {
        if (baseStringImage != "") {
            var sliderImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
            var sliderImageName = baseStringImage.split(';')[0];
        }


        var sliderTitle = $("#sliderTitle").val();
        var id = $("#sliderId").val() == "" || $("#sliderId").val() == null || $("#sliderId").val() == 'NaN' ? 0 : parseInt($("#sliderId").val());

        if (sliderTitle == "" || baseStringImage == "") {
            $("#saveBtn").prop("disabled", false);
            return showMessage();
        }

        var sliderObj = {
            Id: sliderGlobalId,
            SliderTitle: sliderTitle,
            ImageLarge: baseStringImage
        }
        $.ajax({
            url: "SliderHandler.ashx/ProcessRequest?ac=send",
            type: "POST",
            data: JSON.stringify({ sliderObj: sliderObj}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                var d = data;

                if (data > 0) {
                    GetAllSlider();
                    ClearForm();
                    $("#msgBox2").html("Working").removeClass("success").delay(5000).fadeOut();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    alert("failed");
                    $("#msgBox2").html("Failed").addClass("error").show(200);
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            },
            error: function (ex) {
                $("#msgBox2").html("Failed").addClass("error").show(200);
            }
        });
    }
}

function Edit(id) {
    $("#prevImg").attr("src", "");
    var sliderId = $("#slider_" + id).find("td.Id").text();
    sliderGlobalId = sliderId;
    
    $("#sliderId").val(sliderId);
    var img = $("#slider_" + id).find("td.image").html();
    var imgSrc = $(img).attr('src');
    var url = $("#slider_" + id).find("td.imgUrl").text();
    var imageName = $("#slider_" + id).find("td.imageName").text();
    $("#sliderTitle").val($("#slider_" + id).find("td.Title").text());

    $("#sliderImage input:file").val(url);
    $("#imgUrl").val(url);
    $("#imgName").val(imageName);
    $("#prevImg").attr("src", "");
    $("#prevImg").attr("src", imgSrc);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function UpdateSlider() {
    if (baseStringImage=="") {
        baseStringImage = $("#prevImg").attr('src');
    }

    var sliderImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
    var sliderImageName = baseStringImage.split(';')[0];

    var sliderTitle = $("#sliderTitle").val();
    var imgUrl = $("#imgUrl").val();
    var imgName = $("#imgName").val();
    var id = $("#sliderId").val() == "" || $("#sliderId").val() == null || $("#sliderId").val() == 'NaN' ? 0 : parseInt($("#sliderId").val());

    //if (sliderTitle == "" && description == "") {
    //    $("#saveBtn").prop("disabled", false);
    //    return showMessage();
    //}
    var sliderObj = {
        Id: sliderGlobalId,
        SliderTitle: sliderTitle,
        ImageLarge: baseStringImage,
        ImageLargeUrl: imgUrl,
        ImageName: imgName
    }
    $.ajax({
        url: "SliderHandler.ashx/ProcessRequest?ac=send",
        type: "POST",
        data: JSON.stringify({ sliderObj: sliderObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            var d = data;
            if (data > 0) {
                GetAllSlider();
                ClearForm();
                $("#msgBox2").html("Working").removeClass("success").delay(5000).fadeOut();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                $("#saveBtn").text("Save");
                return;
            } else {
                $("#msgBox2").html("Failed").addClass("error").show(200);
                $("#saveBtn").prop("disabled", false);
                return;
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}
function ClearForm() {
    sliderGlobalId = 0;
    baseStringImage = "";
    $("#sliderId").val(0);
    $("#sliderImage").val("");
    $("#sliderTitle").val("");
    $("#imgUrl").val("");
    $("#imgName").val("");
    $("#prevImg").attr('src','');
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#sliderId").val();
    var imgUrl = $("#imgUrl").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "SliderUI.aspx/DeleteASlider",
            type: "POST",
            data: JSON.stringify({ Id: id, imgUrl: imgUrl }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    alert(data.d);
                    GetAllSlider();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
