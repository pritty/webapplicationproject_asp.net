﻿$(document).ready(function () {

    GetAllLogo();
    //$("#saveBtn").click(SaveContact);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
    $("#logoImage").change(function () {
        readURL(this);
        var selectedFile = $("#logoImage")[0].files[0];
        selectedFile.convertToBase64(function (base64) {
            //alert(base64);
            baseStringImage = "";
            blobImage = "";
            blobImage = makeblob(base64);
            baseStringImage = base64;

        });
    });
});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var logoGlobalId = 0;

//*************************Image Manage**************************************
// preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#prevImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function makeblob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);
        return new Blob([raw], { type: contentType });
    }
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
File.prototype.convertToBase64 = function (callback) {
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };
    reader.readAsDataURL(this);
};
//************************************************************
function GetAllLogo() {
    $.ajax({
        url: "LogoHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            LoadLogoTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadLogoTable(data) {
    var tableData = "";
    $("#logoTableBody").empty();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='logoListRow'id=logo_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='MenuId'>" + value.MenuId + "</td>";
            tableData += "<td class='imageName hidden'>" + value.LogoImageName + "</td>";
            tableData += "<td class='imgUrl hidden'>" + value.LogoImageUrl + "</td>";
            tableData += "<td class='image'><img src='data:image/jpg;base64," + value.LogoImage + "' width=300 height=250/></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#logoTableBody").append(tableData);
}


// SAVE PRODUCT
function showMessage() {
    $("#message_box").html("One Item Is Requireds").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
}
function SaveLogo() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);

    if ($("#saveBtn").text() == "Update") {
        UpdateProduct();
    }
    else {
        var logoImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
        var logoImageName = baseStringImage.split(';')[0];

        var menuId = $("#menuId").val();
        var logoImage = $("#logoImage").val();

        var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

        if (menuId == "" || logoImage == "") {
            $("#saveBtn").prop("disabled", false);
            return showMessage();
        }

        var logoObj = {
            Id: logoGlobalId,
            MenuId: menuId,
            LogoImage: baseStringImage
        }
        $.ajax({
            url: "LogoHandler.ashx/ProcessRequest?ac=send",
            type: "POST",
            data: JSON.stringify({ logoObj: logoObj}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                var d = data;
                if (data > 0) {
                    GetAllLogo();
                    ClearForm();
                    $("#msgBox2").html("Working").removeClass("success").fadeOut();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    alert("failed");
                    $("#msgBox2").html("Working").removeClass("success").fadeOut();
                    $("#msgBox2").html("Failed").addClass("error").show(200);
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            },
            error: function (ex) {
                $("#msgBox2").html("Failed").addClass("error").show(200);
            }
        });
    }
}

function Edit(id) {
    $("#prevImg").attr("src", "");
    var varId = $("#logo_" + id).find("td.Id").text();
    logoGlobalId = varId;
    
    $("#varId").val(varId);
    var img = $("#logo_" + id).find("td.image").html();
    var imgSrc = $(img).attr('src');
    var url = $("#logo_" + id).find("td.imgUrl").text();
    var imageName = $("#logo_" + id).find("td.imageName").text();
    $("#menuId").val($("#logo_" + id).find("td.MenuId").text());

    $("#logoImage input:file").val(url);
    $("#imgUrl").val(url);
    $("#imgName").val(imageName);
    $("#prevImg").attr("src", "");
    $("#prevImg").attr("src", imgSrc);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function UpdateProduct() {
    if (baseStringImage=="") {
        baseStringImage = $("#prevImg").attr('src');
    }

    var logoImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
    var logoImageName = baseStringImage.split(';')[0];

    var menuId = $("#menuId").val();

    var imgUrl = $("#imgUrl").val();
    var imgName = $("#imgName").val();
    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    if (menuId == "" || logoImage == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var logoObj = {
        Id: logoGlobalId,
        MenuId: menuId,
        LogoImage: baseStringImage,
        LogoImageName: imgName,
        LogoImageUrl: imgUrl
    }
    $.ajax({
        url: "LogoHandler.ashx/ProcessRequest?ac=send",
        type: "POST",
        data: JSON.stringify({ logoObj: logoObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            var d = data;
            if (data > 0) {
                GetAllLogo();
                ClearForm();
                $("#msgBox2").html("Working").removeClass("success").fadeOut();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                $("#saveBtn").text("Save");
                return;
            } else {
                $("#msgBox2").html("Working").removeClass("success").fadeOut();
                $("#msgBox2").html("Failed").addClass("error").show(200);
                $("#saveBtn").prop("disabled", false);
                return;
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}
function ClearForm() {
    logoGlobalId = 0;
    $("#varId").val(0);
    $("#logoImage").val("");
    $("#menuId").val("");
    $("#imageHeading").val("");
    $("#description").val("");
    $("#imgUrl").val("");
    $("#imgName").val("");
    $("#prevImg").attr('src','');
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#varId").val();
    var imgUrl = $("#imgUrl").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "LogoUI.aspx/DeleteALogo",
            type: "POST",
            data: JSON.stringify({ Id: id, imgUrl: imgUrl }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $("#msgBox2").html("Working").removeClass("success").fadeOut();
                    $('#message_box').html("Deleted").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllLogo();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
