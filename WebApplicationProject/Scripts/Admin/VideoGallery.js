﻿$(document).ready(function () {
    ClearForm();
    GetAllItemList();
    $("#saveBtn").click(SaveVideoGallery);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
});

var autoRowId = 1;
var videoGlobalId = 0;

function GetAllItemList() {
    $.ajax({
        url: "VideoGalleryUI.aspx/GetAllGalleryList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            LoadVideoTable(data.d);
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}

var htmlTextArray;
function LoadVideoTable(data) {
    var tableData = "";
    htmlTextArray = "";
    htmlTextArray = data;
    $("#videoGalleryTable").empty();
    $("#gridViewTable").DataTable().destroy();
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            var src = "", link="";
            if ($(value.VideoUrl, this).attr('src'))
                src = $(value.VideoUrl, this).attr('src');
            else
                src = value.VideoUrl;
            tableData += "<tr class='contactListRow'id=video_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='title'>" + value.Title + "</td>";
            tableData += "<td class='description'>" + value.Description + "</td>";
            tableData += "<td class='videoUrl'><iframe src=" + src + " width=400 height=300 ></iframe></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#videoGalleryTable").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}

function showMessage() {
    $("#message_box").html("video link is Required").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
}
function SaveVideoGallery() {
    $("#saveBtn").prop("disabled", true);
    var title = $("#title").val();
    var description = CKEDITOR.instances['description'].getData();
    var videoUrl = $("#videoUrl").val();
    var id = $("#videoId").val() == "" || $("#videoId").val() == null || $("#videoId").val() == 'NaN' ? 0 : parseInt($("#videoId").val());

    if (videoUrl == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var galleryObj = {
        Id: videoGlobalId,
        Title: title,
        Description: description,
        VideoUrl: videoUrl
    }
    $.ajax({
        url: "VideoGalleryUI.aspx/SaveVideoGallery",
        type: "POST",
        data: JSON.stringify({ galleryObj: galleryObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            GetAllItemList();
            if (data.d == "Updated") {
                ClearForm();
                $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                $("#msgBox2").hide();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
            else if (data.d == "Failed") {
                $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                $("#msgBox2").hide();
                $('#message_box').html("Failed").addClass("alert alert-Danger alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            } else {
                if (data.d == "Saved") {
                    ClearForm();
                    $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                    $("#msgBox2").hide();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
        }
    });
}

function Edit(id) {
    var videoId = $("#video_" + id).find("td.Id").text();
    videoGlobalId = videoId;
    $("#videoId").val(videoId);

    var dataRow = htmlTextArray.filter(function (obj) {
        return (obj.Id == videoId);
    });

    //var url = $("#video_" + id).find("td.videoUrl").html();
    var url = dataRow[0].VideoUrl;
    $("#title").val($("#video_" + id).find("td.title").text());
    CKEDITOR.instances.description.setData(dataRow[0].Description);
    $("#videoUrl").val(url);

    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    videoGlobalId = 0;
    $("#videoId").val(0);
    $("#title").val("");
    CKEDITOR.instances.description.setData('');
    $("#videoUrl").val("");
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#videoId").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "VideoGalleryUI.aspx/DeleteGallery",
            type: "POST",
            data: JSON.stringify({ Id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    ClearForm();
                    $('#message_box').html("Deleted").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllItemList();
                   
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
