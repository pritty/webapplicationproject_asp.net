﻿$(document).ready(function () {

    GetAllRecipe();
    //$("#saveBtn").click(SaveContact);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
    $("#recipeImage").change(function () {
        readURL(this);
        var selectedFile = $("#recipeImage")[0].files[0];
        selectedFile.convertToBase64(function (base64) {
            //alert(base64);
            baseStringImage = "";
            blobImage = "";
            blobImage = makeblob(base64);
            baseStringImage = base64;

        });
    });
});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var productGlobalId = 0;

//*************************Image Manage**************************************
// preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#prevImg").attr("src", "");
            $('#prevImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function makeblob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);
        return new Blob([raw], { type: contentType });
    }
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
File.prototype.convertToBase64 = function (callback) {
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };
    reader.readAsDataURL(this);
};
//************************************************************
var htmlTextArray;
function GetAllRecipe() {
    $.ajax({
        url: "RecipeHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            htmlTextArray = data;
            LoadDataTable(data);
        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadDataTable(data) {
    var tableData = "";
    $("#recipeTableBody").empty();
    $("#gridViewTable").DataTable().destroy();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            var src = "", link = "";
            if ($(value.VideoUrl, this).attr('src'))
                src = $(value.VideoUrl, this).attr('src');
            else
                link = value.VideoUrl;
            tableData += "<tr class='contactListRow'id=recipe_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Title'>" + value.Title + "</td>";
            tableData += "<td class='SubTitle'>" + value.SubTitle + "</td>";
            tableData += "<td class='MadeByRecipie'>" + value.MadeByRecipie + "</td>";
            tableData += "<td class='ShortDescription'>" + value.ShortDescription + "</td>";
            tableData += "<td class='DetailsDescription hidden'>" + value.DetailsDescription + "</td>";
            tableData += "<td class='ImageName hidden'>" + value.ImageName + "</td>";
            tableData += "<td class='ImageUrl hidden'>" + value.ImageUrl + "</td>";

            tableData += "<td class='ImageData hidden'><img src='data:image/jpg;base64," + value.ImageDataString + "' width=300 height=250/></td>";

                tableData += "<td class='VideoUrl hidden'>" + value.VideoUrl + "</iframe></td>";

            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#recipeTableBody").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}


// SAVE PRODUCT
function showMessage() {
    $("#message_box").html("Title,Made By Recipie and Short Description Required").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
}
function SaveRecipe() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);

    if ($("#saveBtn").text() == "Update") {
        UpdateProduct();
    }
    else {
        if (baseStringImage != "") {
            var productImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
            var productImageName = baseStringImage.split(';')[0];
        }


        var title = $("#title").val();
        var subTitle = $("#subTitle").val();
        var madeByRecipie = $("#madeByRecipie").val();
        var videoUrl = $("#videoUrl").val();
        var shortDescription = CKEDITOR.instances['shortDescription'].getData();
        var detailsDescription = CKEDITOR.instances['detailsDescription'].getData();

        var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

        if (title == "" || madeByRecipie == "" || shortDescription == "") {
            $("#saveBtn").prop("disabled", false);
            return showMessage();
        }

        var recipeObj = {
            Id: productGlobalId,
            Title: title,
            SubTitle: subTitle,
            MadeByRecipie: madeByRecipie,
            ShortDescription: shortDescription,
            DetailsDescription: detailsDescription,
            ImageDataString: baseStringImage,
            VideoUrl: videoUrl
        }
        $.ajax({
            url: "RecipeHandler.ashx/ProcessRequest?ac=send",
            type: "POST",
            data: JSON.stringify({ recipeObj: recipeObj}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                var d = data;
                if (data > 0) {
                    GetAllRecipe();
                    ClearForm();
                    $("#msgBox2").html("").removeClass("success").fadeOut().delay(5000).fadeOut();
                    //$("#msgBox2").hide();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    alert("failed");
                    $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            },
            error: function (ex) {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
            }
        });
    }
}

function Edit(id) {
    $("#prevImg").attr("src", "");
    var varId = $("#recipe_" + id).find("td.Id").text();
    productGlobalId = varId;  
    $("#varId").val(varId);

    var dataRow = htmlTextArray.filter(function (obj) {
        return (obj.Id == varId);
    });

    var img = $("#recipe_" + id).find("td.ImageData").html();
    var imgSrc = $(img).attr('src');
    var imageUrl = $("#recipe_" + id).find("td.ImageUrl").text();
    var imageName = $("#recipe_" + id).find("td.ImageName").text();
    var videoUrl = $("#recipe_" + id).find("td.VideoUrl").html();

    CKEDITOR.instances.shortDescription.setData(dataRow[0].ShortDescription);
    CKEDITOR.instances.detailsDescription.setData(dataRow[0].DetailsDescription);

    $("#title").val($("#recipe_" + id).find("td.Title").text());
    $("#subTitle").val($("#recipe_" + id).find("td.SubTitle").text());
    $("#madeByRecipie").val($("#recipe_" + id).find("td.MadeByRecipie").text());
    $("#videoUrl").val(videoUrl);
    $("#recipeImage input:file").val(imageUrl);
    $("#imgUrl").val(imageUrl);
    $("#prevImg").attr("src", "");
    $("#imgName").val(imageName);
    $("#prevImg").attr("src", imgSrc);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function UpdateProduct() {
    if (baseStringImage=="") {
        baseStringImage = $("#prevImg").attr('src');
    }

    var productImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
    var productImageName = baseStringImage.split(';')[0];

    var imgUrl = $("#imgUrl").val();
    var imgName = $("#imgName").val();
    var title = $("#title").val();
    var subTitle = $("#subTitle").val();
    var madeByRecipie = $("#madeByRecipie").val();
    var videoUrl = $("#videoUrl").val();
    var shortDescription = CKEDITOR.instances['shortDescription'].getData();
    var detailsDescription = CKEDITOR.instances['detailsDescription'].getData();

    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    if (title == "" && subTitle == "" && madeByRecipie == "" && shortDescription == "" && detailsDescription == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var recipeObj = {
        Id: productGlobalId,
        Title: title,
        SubTitle: subTitle,
        MadeByRecipie: madeByRecipie,
        ShortDescription: shortDescription,
        DetailsDescription: detailsDescription,
        ImageDataString: baseStringImage,
        ImageName: imgName,
        ImageUrl: imgUrl,
        VideoUrl: videoUrl
    }
    $.ajax({
        url: "RecipeHandler.ashx/ProcessRequest?ac=send",
        type: "POST",
        data: JSON.stringify({ recipeObj: recipeObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            var d = data;
            if (data > 0) {
                GetAllRecipe();
                ClearForm();
                $("#msgBox2").html("Working").removeClass("success").delay(5000).fadeOut();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                $("#saveBtn").text("Save");
                return;
            } else {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}
function ClearForm() {
    productGlobalId = 0;
    baseStringImage = "";
    $("#varId").val(0);
    $("#title").val("");
    $("#subTitle").val("");
    $("#madeByRecipie").val("");
    $("#videoUrl").val("");
    $("#recipeImage").val("");
    CKEDITOR.instances.shortDescription.setData('');
    CKEDITOR.instances.detailsDescription.setData('');
    $("#imgUrl").val("");
    $("#imgName").val("");
    $("#prevImg").attr('src','');
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#varId").val();
    var imgUrl = $("#imgUrl").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "RecipeUI.aspx/DeleteARecipe",
            type: "POST",
            data: JSON.stringify({ Id: id, imgUrl: imgUrl }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $('#message_box').html("Deleted").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllRecipe();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
