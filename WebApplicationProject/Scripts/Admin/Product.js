﻿$(document).ready(function () {
    ClearForm();
    GetAllProduct();
    //$("#saveBtn").click(SaveContact);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
    $("#productImage").change(function () {
        readURL(this);
        var selectedFile = $("#productImage")[0].files[0];
        selectedFile.convertToBase64(function (base64) {
            //alert(base64);
            baseStringImage = "";
            blobImage = "";
            blobImage = makeblob(base64);
            baseStringImage = base64;

        });
    });
});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var productGlobalId = 0;

//*************************Image Manage**************************************
// preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#prevImg").attr("src", "");
            $('#prevImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function makeblob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);
        return new Blob([raw], { type: contentType });
    }
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
File.prototype.convertToBase64 = function (callback) {
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };
    reader.readAsDataURL(this);
};
//************************************************************
function GetAllProduct() {
    $.ajax({
        url: "ProductHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            LoadProductTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadProductTable(data) {
    var tableData = "";
    $("#productTableBody").empty();
    $("#gridViewTable").DataTable().destroy();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='contactListRow'id=product_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Title'>" + value.ProductTitle + "</td>";
            tableData += "<td class='Heading'>" + value.ProductImageHeading + "</td>";
            tableData += "<td class='imageName hidden'>" + value.ProductImageName + "</td>";
            tableData += "<td class='description'>" + value.Description + "</td>";
            tableData += "<td class='imgUrl hidden'>" + value.ProductImageUrl + "</td>";
            tableData += "<td class='image'><img src='data:image/jpg;base64," + value.ProductImage + "' width=300 height=250/></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#productTableBody").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}


// SAVE PRODUCT
function showMessage() {
    $("#message_box").html("Title, Product Image and Description Required").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
}
function SaveProduct() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);

    if ($("#saveBtn").text() == "Update") {
        UpdateProduct();
    }
    else {
        if (baseStringImage!="") {
        var productImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
        var productImageName = baseStringImage.split(';')[0]; 
        }


        var productTitle = $("#productTitle").val();
        var imageHeading = $("#imageHeading").val();
        var description = $("#description").val();
        var id = $("#productId").val() == "" || $("#productId").val() == null || $("#productId").val() == 'NaN' ? 0 : parseInt($("#productId").val());

        if (productTitle == "" || description == "" || baseStringImage=="") {
            $("#saveBtn").prop("disabled", false);
            return showMessage();
        }

        var productObj = {
            Id: productGlobalId,
            ProductTitle: productTitle,
            ProductImageHeading: imageHeading,
            ProductImage: baseStringImage,
            Description: description
        }
        $.ajax({
            url: "ProductHandler.ashx/ProcessRequest?ac=send",
            type: "POST",
            data: JSON.stringify({ productObj: productObj }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                var d = data;

                if (data > 0) {
                    GetAllProduct();
                    ClearForm();
                    $("#msgBox2").html("").removeClass("success").fadeOut();
                    $("#msgBox2").hide();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    alert("failed");
                    $("#msgBox2").html("Failed").addClass("error").show(200);
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            },
            error: function (ex) {
                $("#msgBox2").html("Failed").addClass("error").show(200);
            }
        });
    }
}

function Edit(id) {
    $("#prevImg").attr("src", "");
    var productId = $("#product_" + id).find("td.Id").text();
    productGlobalId = productId;

    $("#productId").val(productId);
    var img = $("#product_" + id).find("td.image").html();
    var imgSrc = $(img).attr('src');
    var url = $("#product_" + id).find("td.imgUrl").text();
    var imageName = $("#product_" + id).find("td.imageName").text();
    $("#productTitle").val($("#product_" + id).find("td.Title").text());
    $("#imageHeading").val($("#product_" + id).find("td.Heading").text());
    $("#description").val($("#product_" + id).find("td.description").text());
    $("#productImage input:file").val(url);
    $("#imgUrl").val(url);
    $("#imgName").val(imageName);
    $("#prevImg").attr("src", "");
    $("#prevImg").attr("src", imgSrc);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function UpdateProduct() {
    if (baseStringImage == "") {
        baseStringImage = $("#prevImg").attr('src');
    }

    var productImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
    var productImageName = baseStringImage.split(';')[0];

    var productTitle = $("#productTitle").val();
    var imageHeading = $("#imageHeading").val();
    var description = $("#description").val();
    var imgUrl = $("#imgUrl").val();
    var imgName = $("#imgName").val();
    var id = $("#productId").val() == "" || $("#productId").val() == null || $("#productId").val() == 'NaN' ? 0 : parseInt($("#productId").val());

    if (productTitle == "" && description == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var productObj = {
        Id: productGlobalId,
        ProductTitle: productTitle,
        ProductImageHeading: imageHeading,
        ProductImage: baseStringImage,
        ProductImageName: baseStringImage,
        ProductImageUrl: imgUrl,
        Description: description
    }
    $.ajax({
        url: "ProductHandler.ashx/ProcessRequest?ac=send",
        type: "POST",
        data: JSON.stringify({ productObj: productObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            var d = data;
            if (data > 0) {
                GetAllProduct();
                ClearForm();
                $("#msgBox2").html("").removeClass("success").fadeOut();
                $("#msgBox2").hide();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                $("#saveBtn").text("Save");
                return;
            } else {
                $("#msgBox2").html("Failed").addClass("error").show(200);
                $("#saveBtn").prop("disabled", false);
                return;
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}
function ClearForm() {
    productGlobalId = 0;
    baseStringImage = "";
    $("#productId").val(0);
    $("#productImage").val("");
    $("#productTitle").val("");
    $("#imageHeading").val("");
    $("#description").val("");
    $("#imgUrl").val("");
    $("#imgName").val("");
    $("#prevImg").attr('src', '');
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#productId").val();
    var imgUrl = $("#imgUrl").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "ProductsUI.aspx/DeleteAProduct",
            type: "POST",
            data: JSON.stringify({ Id: id, imgUrl: imgUrl }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    alert(data.d);
                    GetAllProduct();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
