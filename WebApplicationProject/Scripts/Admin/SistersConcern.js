﻿$(document).ready(function () {

    GetAllSistersConcern();
    //$("#saveBtn").click(SaveContact);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
    //$("#productImage").change(function () {
    //    readURL(this);
    //    var selectedFile = $("#productImage")[0].files[0];
    //    selectedFile.convertToBase64(function (base64) {
    //        //alert(base64);
    //        baseStringImage = "";
    //        blobImage = "";
    //        blobImage = makeblob(base64);
    //        baseStringImage = base64;

    //    });
    //});
});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var varGlobalId = 0;

//*************************Image Manage**************************************
// preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#prevImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function makeblob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);
        return new Blob([raw], { type: contentType });
    }
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
File.prototype.convertToBase64 = function (callback) {
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };
    reader.readAsDataURL(this);
};
//************************************************************
function GetAllSistersConcern() {
    $.ajax({
        url: "SistersConcernHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            LoadDataTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadDataTable(data) {
    var tableData = "";
    $("#sistersConcernDataTable").empty();
    $("#gridViewTable").DataTable().destroy();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='sistersConcerntListRow'id=sc_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='CompanyName'>" + value.CompanyName + "</td>";
            tableData += "<td class='Address'>" + value.Address + "</td>";
            tableData += "<td class='Email'>" + value.Email + "</td>";
            tableData += "<td class='MobileNo1'>" + value.MobileNo1 + "</td>";
            tableData += "<td class='MobileNo2'>" + value.MobileNo2 + "</td>";
            tableData += "<td class='MobileNo3'>" + value.MobileNo3 + "</td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#sistersConcernDataTable").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}


// SAVE PRODUCT
function showMessage() {
    $("#message_box").html("Company Name,Address, Mobile and Email Is Requireds").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
}
function SaveSistersConcern() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);

    if ($("#saveBtn").text() == "Update") {
        UpdateProduct();
    }
    else {
        //var productImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
        //var productImageName = baseStringImage.split(';')[0];

        var companyName = $("#companyName").val();
        var address = $("#address").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var mobile2 = $("#mobile2").val();
        var phone = $("#phone").val();
        var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

        if (companyName == "" || email == "" || address == "" || mobile == "") {
            $("#saveBtn").prop("disabled", false);
            return showMessage();
        }

        var sisterConcernObj = {
            Id: varGlobalId,
            CompanyName: companyName,
            Address: address,
            Email: email,
            MobileNo1: mobile,
            MobileNo2: mobile2,
            MobileNo3: phone
        }
        $.ajax({
            url: "SistersConcernHandler.ashx/ProcessRequest?ac=send",
            type: "POST",
            data: JSON.stringify({ sisterConcernObj: sisterConcernObj}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                var d = data;
                if (data > 0) {
                    GetAllSistersConcern();
                    ClearForm();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    alert("failed");
                    $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            },
            error: function (ex) {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
            }
        });
    }
}

function Edit(id) {
    //$("#prevImg").attr("src", "");
    var varId = $("#sc_" + id).find("td.Id").text();
    varGlobalId = varId;
    
    $("#varId").val(varId);
    //var img = $("#sc_" + id).find("td.image").html();
    //var imgSrc = $(img).attr('src');
    //var url = $("#sc_" + id).find("td.imgUrl").text();
    //var imageName = $("#sc_" + id).find("td.imageName").text();
    $("#companyName").val($("#sc_" + id).find("td.CompanyName").text());
    $("#address").val($("#sc_" + id).find("td.Address").text());
    $("#email").val($("#sc_" + id).find("td.Email").text());
    $("#mobile").val($("#sc_" + id).find("td.MobileNo1").text());
    $("#mobile2").val($("#sc_" + id).find("td.MobileNo2").text());
    $("#phone").val($("#sc_" + id).find("td.MobileNo3").text());
    //$("#productImage input:file").val(url);
    //$("#imgUrl").val(url);
    //$("#imgName").val(imageName);
    //$("#prevImg").attr("src", imgSrc);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function UpdateProduct() {
    //if (baseStringImage=="") {
    //    baseStringImage = $("#prevImg").attr('src');
    //}

    //var productImageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
    //var productImageName = baseStringImage.split(';')[0];

    var companyName = $("#companyName").val();
    var address = $("#address").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var mobile2 = $("#mobile2").val();
    var phone = $("#phone").val();
    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    if (companyName == "" && email == "" && address == "" && mobile == "" && phone == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var sisterConcernObj = {
        Id: varGlobalId,
        CompanyName: companyName,
        Address: address,
        Email: email,
        MobileNo1: mobile,
        MobileNo2: mobile2,
        MobileNo3: phone
    }
    $.ajax({
        url: "SistersConcernHandler.ashx/ProcessRequest?ac=send",
        type: "POST",
        data: JSON.stringify({ sisterConcernObj: sisterConcernObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            var d = data;
            if (data > 0) {
                GetAllSistersConcern();
                ClearForm();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                $("#saveBtn").text("Save");
                return;
            } else {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
        }
    });
}
function ClearForm() {
    varGlobalId = 0;
    $("#varId").val(0);
    //$("#productImage").val("");
    $("#companyName").val("");
    $("#address").val("");
    $("#email").val("");
    $("#mobile").val("");
    $("#mobile2").val("");
    $("#phone").val("");

    //$("#imgUrl").val("");
    //$("#imgName").val("");
    //$("#prevImg").attr('src','');
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#varId").val();
    //var imgUrl = $("#imgUrl").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "SistersConcernUI.aspx/DeleteASistersConcern",
            type: "POST",
            data: JSON.stringify({ Id: id}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $('#message_box').html("Deleted").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllSistersConcern();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
