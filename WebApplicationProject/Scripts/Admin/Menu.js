﻿$(document).ready(function () {

    GetAllItemList();
    $("#saveBtn").click(SaveMenu);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
});

var autoRowId = 1;
var itemGlobalId = 0;

function GetAllItemList() {
    $.ajax({
        url: "MenuUI.aspx/GetAllMenuItemList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            LoadMenuTable(data.d);
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}


function LoadMenuTable(data) {
    var tableData = "";
    $("#menuTableBody").empty();
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            var src = "";
            if ($(value.MapUrl, this).attr('src'))
                 src = $(value.MapUrl, this).attr('src');
            else
                src = value.MapUrl;
            tableData += "<tr class='menuListRow'id=menu_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='MenuTitle'>" + value.MenuTitle + "</td>";
            tableData += "<td class='MenuUrl'>" + value.MenuUrl + "</td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#menuTableBody").append(tableData);
}

function showMessage() {
    $("#message_box").html("One Item Is Requireds").fadeIn(1000).delay(5000).fadeOut();
}
function SaveMenu() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);
    var menuTitle = $("#menuTitle").val();
    var menuUrl = $("#menuUrl").val();
    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    if (menuTitle == "" || menuUrl == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var menuObj = {
        Id: itemGlobalId,
        MenuTitle: menuTitle,
        MenuUrl: menuUrl
    }
    $.ajax({
        url: "MenuUI.aspx/SaveMenu",
        type: "POST",
        data: JSON.stringify({ menuObj: menuObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            GetAllItemList();
            if (data.d == "Updated") {
                ClearForm();
                $("#msgBox2").html("Working").removeClass("success").fadeOut();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
            else if (data.d == "Failed") {
                $("#msgBox2").html("Working").removeClass("success").fadeOut();
                $('#message_box').html("Failed").addClass("alert alert-Danger alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            } else {
                if (data.d == "Saved") {
                    ClearForm();
                    $("#msgBox2").html("Working").removeClass("success").fadeOut();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}

function Edit(id) {
    var varId = $("#menu_" + id).find("td.Id").text();
    itemGlobalId = varId;
    $("#varId").val(varId);

    $("#menuTitle").val($("#menu_" + id).find("td.MenuTitle").text());
    $("#menuUrl").val($("#menu_" + id).find("td.MenuUrl").text());

    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    itemGlobalId = 0;
    $("#varId").val(0);
    $("#menuTitle").val("");
    $("#menuUrl").val("");
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#varId").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "MenuUI.aspx/DeleteAMenu",
            type: "POST",
            data: JSON.stringify({ Id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $("#msgBox2").html("Working").removeClass("success").fadeOut();
                    $('#message_box').html("Deleted").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllItemList();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
