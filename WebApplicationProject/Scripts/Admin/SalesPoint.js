﻿$(document).ready(function () {

    GetAllSalesPointList();
    $("#saveBtn").click(SaveSalesPoint);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
});

var autoRowId = 1;
var itemGlobalId = 0;

function GetAllSalesPointList() {
    $.ajax({
        url: "SalesPointUI.aspx/GetAllSalesPointList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            LoadSalesPointTable(data.d);
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}


function LoadSalesPointTable(data) {
    var tableData = "";
    $("#salesPointTableBody").empty();
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='salesPointListRow'id=salesPoint_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='BranchId'>" + value.BranchId + "</td>";
            tableData += "<td class='BranchName'>" + value.BranchName + "</td>";
            tableData += "<td class='ManagerName'>" + value.ManagerName + "</td>";
            tableData += "<td class='Address'>" + value.Address + "</td>";
            tableData += "<td class='Mobile'>" + value.Mobile + "</td>";
            tableData += "<td class='Phone'>" + value.Phone + "</td>";
            tableData += "<td class='WebAddress'>" + value.WebAddress + "</td>";
            tableData += "<td class='Email'>" + value.Email + "</td>";
            tableData += "<td class='MapUrl hidden'>" + value.MapUrl + "</td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#salesPointTableBody").append(tableData);
}

function showMessage() {
    $("#message_box").html("Sales Point Name, Mobile and Address Is Requireds").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
}
function SaveSalesPoint() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);
    var branchId = $("#branchId").val();
    var salesPointName = $("#salesPointName").val();
    var managerName = $("#managerName").val();
    var mobile = $("#mobile").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var webAddress = $("#webAddress").val();
    var address = $("#address").val();
    var mapUrl = $("#mapUrl").val();
    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    if (salesPointName == "" || mobile == "" || address == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var salesPointObj = {
        Id: itemGlobalId,
        BranchId: branchId,
        BranchName: salesPointName,
        Address: address,
        ManagerName: managerName,
        Mobile: mobile,
        Phone: phone,
        WebAddress: webAddress,
        Email: email,
        MapUrl: mapUrl
    }
    $.ajax({
        url: "SalesPointUI.aspx/SaveSalesPoint",
        type: "POST",
        data: JSON.stringify({ salesPointObj: salesPointObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            GetAllSalesPointList();
            if (data.d == "Updated") {
                ClearForm();
                $("#msgBox2").removeClass("success").delay(5000).fadeOut();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
            else if (data.d == "Failed") {
                $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                //$("#msgBox2").hide();
                $('#message_box').html("Failed").addClass("alert alert-Danger alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            } else {
                if (data.d == "Saved") {
                    ClearForm();
                    $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                    $("#msgBox2").hide();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
        }
    });
}

function Edit(id) {
    var varId = $("#salesPoint_" + id).find("td.Id").text();
    itemGlobalId = varId;
    $("#varId").val(varId);

    $("#branchId").val($("#salesPoint_" + id).find("td.BranchId").text());
    $("#salesPointName").val($("#salesPoint_" + id).find("td.BranchName").text());
    $("#managerName").val($("#salesPoint_" + id).find("td.ManagerName").text());
    $("#address").val($("#salesPoint_" + id).find("td.Address").text());
    $("#mobile").val($("#salesPoint_" + id).find("td.Mobile").text());
    $("#phone").val($("#salesPoint_" + id).find("td.Phone").text());
    $("#webAddress").val($("#salesPoint_" + id).find("td.WebAddress").text());
    $("#email").val($("#salesPoint_" + id).find("td.Email").text());
    $("#mapUrl").val($("#salesPoint_" + id).find("td.MapUrl").text());

    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    itemGlobalId = 0;
    $("#varId").val(0);
    $("#salesPointName").val("");
    $("#branchId").val("");
    $("#managerName").val("");
    $("#address").val("");
    $("#mobile").val("");
    $("#phone").val("");
    $("#webAddress").val("");
    $("#email").val("");
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#varId").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "SalesPointUI.aspx/DeleteSalesPoint",
            type: "POST",
            data: JSON.stringify({ Id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $('#message_box').html("Deleted").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllSalesPointList();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
