﻿$(document).ready(function () {

    GetAllItemList();
    $("#saveBtn").click(SaveAbout);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(DeleteAbout);
    $("#deleteBtn").hide();
    $("#message_box").hide();
});

var autoRowId = 1;
var itemGlobalId = 0;

var htmlTextArray;
function GetAllItemList() {
    $.ajax({
        url: "About.aspx/GetAllItemList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            htmlTextArray = data.d;
            LoadAboutTable(data.d);
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}


function LoadAboutTable(data) {
    var tableData = "";
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='aboutListRow'id=about_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='description'>" + value.CompanyDescription + "</td>";
            tableData += "<td class='mission'>" + value.Mission + "</td>";
            tableData += "<td class='vission'>" + value.Vission + "</td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#aboutTableBody").empty();
    $("#aboutTableBody").append(tableData);
}

function showMessage() {
    $("#message_box").html("One Item Is Requireds").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
}
function SaveAbout() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);
    var description = CKEDITOR.instances['companyDescription'].getData(); 
    var mission = CKEDITOR.instances['mission'].getData(); 
    var vission = CKEDITOR.instances['vission'].getData(); 
    var id = $("#aboutId").val() == "" || $("#aboutId").val() == null || $("#aboutId").val() == 'NaN' ? 0 : parseInt($("#aboutId").val());

    if (description == "" && mission == "" && vission == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var aboutObj = {
        Id: id,
        CompanyDescription: description,
        Mission: mission,
        Vission: vission
    }
    $.ajax({
        url: "About.aspx/SaveAbout",
        type: "POST",
        data: JSON.stringify({ aboutObj: aboutObj}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            GetAllItemList();
            if (data.d == "Updated") {
                ClearForm();
                $("#msgBox2").html("").removeClass("success").fadeOut();
                $("#msgBox2").hide();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            else if (data.d == "Failed") {
                $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                //$("#msgBox2").hide();
                $('#message_box').html("Failed").addClass("alert alert-warning alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    if (data.d == "Saved") {
                        ClearForm();
                        $("#msgBox2").html("").removeClass("success").fadeOut();
                        $("#msgBox2").hide();
                        $('#message_box').html("Saved").addClass("alert alert-warning alert-dismissible").show(200);
                        $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                        $("#saveBtn").prop("disabled", false);                        
                        return;
                    }
                }           
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}

function Edit(id) {
    var aboutId = $("#about_" + id).find("td.Id").text();
    var dataRow = htmlTextArray.filter(function (obj) {
        return (obj.Id == aboutId);
    });

    $("#aboutId").val(aboutId);

    //var description = $("#about_" + id).find("td.description").text();
    //var mission = $("#about_" + id).find("td.Mission").text();
    //var vission = $("#about_" + id).find("td.vission").text();

    CKEDITOR.instances.companyDescription.setData(dataRow[0].CompanyDescription);
    CKEDITOR.instances.mission.setData(dataRow[0].Mission);
    CKEDITOR.instances.vission.setData(dataRow[0].Vission);
    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    itemGlobalId = 0;
    $("#aboutId").val(0);
    CKEDITOR.instances.companyDescription.setData('');
    CKEDITOR.instances.mission.setData('');
    CKEDITOR.instances.vission.setData('');
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function DeleteAbout() {
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "About.aspx/DeleteAbout",
            type: "POST",
            data: JSON.stringify({ Id: $("#aboutId").val() }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $('#message_box').html("Deleted").addClass("alert alert-warning alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllItemList();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}





function UpdateConsumption() {

    var consumptionQuantity = parseFloat($("#consumptionQuantity").val());
    var wastageUnit = parseFloat($("#wastageUnit").val());
    var totalQuantity = parseFloat(consumptionQuantity + wastageUnit).toFixed(2);


    //$("#pi_" + PurchaseCurrentId + " td.supplierId").html($("#supplierId").val());
    //$("#pi_" + PurchaseCurrentId + " td.chalanNo").html($("#chalanNo").val());
    //$("#pi_" + PurchaseCurrentId + " td.remarks").html($("#remarks").val());
    $("#con_" + ConRowIdGlobal + " td.rawItemCode").html($("#rawItemName option:selected").val());
    $("#con_" + ConRowIdGlobal + " td.rawItemName").html($("#rawItemName option:selected").text());
    $("#con_" + ConRowIdGlobal + " td.consumptionQuantity").html($("#consumptionQuantity").val());
    $("#con_" + ConRowIdGlobal + " td.consumptionUnit").html($("#consumptionUnit option:selected").text());
    $("#con_" + ConRowIdGlobal + " td.wastagePercent").html($("#wastagePercent").val());
    $("#con_" + ConRowIdGlobal + " td.wastageUnit").html($("#wastageUnit").val());
    $("#con_" + ConRowIdGlobal + " td.totalQuantity").html(totalQuantity);
    $("#con_" + ConRowIdGlobal + " td.totalUnit").html();
    $("#AddeditBtn").text("Add");
    //$("#UpdateClearBtn").hide();
    //UpdateClear();
}

function DeleteConsumption(id) {
    // $("#finishGoodsCode").val($("#about_" + id).find("td.VarItemCode").text());
    // $("#finishGoodsName").val($("#about_" + id).find("td.VarItemName").text());
    var con = confirm("Are you sure, you want to delete");
    if (con == true) {
        var itemCode = $("#con_" + id).find("td.rawItemCode").text();
        insertConId = 1;
        $("#con_" + id).remove();
        //DeleteSessionDataWhenItemDelete(itemCode);
        // RefreshProductTable();
    }

}