﻿$(document).ready(function () {

GetAllItemList();
$("#saveBtn").click(SaveContact);
$("#clearBtn").click(ClearForm);
$("#deleteBtn").click(Delete);
$("#deleteBtn").hide();
$("#message_box").hide();
});

var autoRowId = 1;
var itemGlobalId = 0;
var contactDataArray;
function GetAllItemList() {
$.ajax({
    url:"ContactUI.aspx/GetAllContactItemList",
    type:"POST",
    data:JSON.stringify({}),
    contentType:"application/json; charset=utf-8",
    dataType:"json",
    success:function(data) {
        contactDataArray = "";
        contactDataArray = data.d;
        LoadContactTable(data.d);
    },
    error:function (ex) {
        alert(ex.message);
    }
});
}


function LoadContactTable(data) {
    var tableData = "";
    $("#contactTableBody").empty();
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            var src = "";
            if ($(value.MapUrl, this).attr('src'))
                 src = $(value.MapUrl, this).attr('src');
            else
                src = value.MapUrl;
            tableData += "<tr class='contactListRow'id=contact_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Heading'>" + value.Heading + "</td>";
            tableData += "<td class='Address'>" + value.Address + "</td>";
            tableData += "<td class='Address2'>" + value.Address2 + "</td>";
            tableData += "<td class='Phone'>" + value.Phone + "</td>";
            tableData += "<td class='Phone2'>" + value.Phone2 + "</td>";
            tableData += "<td class='Mobile'>" + value.Mobile + "</td>";
            tableData += "<td class='Mobile2'>" + value.Mobile2 + "</td>";
            tableData += "<td class='MapUrl'><iframe src=" + src + " width=400 height=300 ></iframe></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#contactTableBody").append(tableData);
}

function showMessage() {
    $("#message_box").html("Address and Mobile Required").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
}
function SaveContact() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);
    var heading = $("#heading").val();
    var address = $("#address").val();
    var address2 = $("#address2").val();
    var phon = $("#phon").val();
    var phon2 = $("#phon2").val();
    var mobile = $("#mobile").val();
    var mobile2 = $("#mobile2").val();
    var mapUrl = $("#mapUrl").val();
    var id = $("#contactId").val() == "" || $("#contactId").val() == null || $("#contactId").val() == 'NaN' ? 0 : parseInt($("#contactId").val());

    if (address == "" || mobile == "") {
        if (address == "" || mobile == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
        }
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var contactObj = {
        Id: itemGlobalId,
        Heading: heading,
        Address: address,
        Address2: address2,
        Phone: phon,
        Phone2: phon2,
        Mobile: mobile,
        Mobile2: mobile2,
        MapUrl: mapUrl
    }
    $.ajax({
        url: "ContactUI.aspx/SaveContact",
        type: "POST",
        data: JSON.stringify({ contactObj: contactObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            alert(data.d);
            GetAllItemList();
            if (data.d == "Updated") {
                ClearForm();
                $("#msgBox2").html("Updated").addClass("success").show(200).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
            else if (data.d == "Failed") {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            } else {
                if (data.d == "Saved") {
                    ClearForm();
                    $("#msgBox2").html("Saved").addClass("success").show(200).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
            $("#saveBtn").prop("disabled", false);
        }
    });
}

function Edit(id) {
    var contactId = $("#contact_" + id).find("td.Id").text();
    itemGlobalId = contactId;
    $("#contactId").val(contactId);
    var filterUrl = contactDataArray.filter(function(obj) {
        return (obj.Id == contactId);
    });
    //var url = $("#contact_" + id).find("td.MapUrl").html();
    var url = filterUrl[0].MapUrl;
    $("#heading").val($("#contact_" + id).find("td.Heading").text());
    $("#address").val($("#contact_" + id).find("td.Address").text());
    $("#address2").val($("#contact_" + id).find("td.Address2").text());
    $("#phon").val($("#contact_" + id).find("td.Phone").text());
    $("#phon2").val($("#contact_" + id).find("td.Phone2").text());
    $("#mobile").val($("#contact_" + id).find("td.Mobile").text());
    $("#mobile2").val($("#contact_" + id).find("td.Mobile2").text());
    $("#mapUrl").val(url);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    itemGlobalId = 0;
    $("#contactId").val(0);
    $("#heading").val("");
    $("#address").val("");
    $("#address2").val("");
    $("#phon").val("");
    $("#phon2").val("");
    $("#mobile").val("");
    $("#mobile2").val("");
    $("#mapUrl").val("");
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#contactId").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "ContactUI.aspx/DeleteContact",
            type: "POST",
            data: JSON.stringify({ Id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    alert(data.d);
                    GetAllItemList();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
