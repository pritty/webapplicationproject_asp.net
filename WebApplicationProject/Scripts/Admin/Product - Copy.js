﻿$(document).ready(function () {

    GetAllProduct();

});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var productGlobalId = 0;

function GetAllProduct() {
    $.ajax({
        url: "Product.aspx/GetAllProduct",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            LoadProductServiceBox(data);
            //LoadProductTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadProductServiceBox(data) {
    var serviceBox = "";
    $("#serviceBox").html('');
    var html = "";
    if (data!=""||data!=null) {
        $.each(data, function (key, value) {
            html+="<div class='col-sm-6 col-md-4 serviceBox'>" +
                "<a  href='Product_Details.aspx?id=" + value.Id + "' class='serviceTitle'>" +
                "<img src='../../uploads/Products" + value.ProductImageName + "' class='img img-responsive img-circle center-block' alt='" + value.ProductTitle + "' />" +
                "" + value.ProductTitle + "" +
                "</a></div>";
            autoRowId++;
        });
    }
    $("#serviceBox").html(html);
}
function LoadProductTable(data) {
    var tableData = "";
    $("#productTableBody").empty();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='contactListRow'id=product_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Title'>" + value.ProductTitle + "</td>";
            tableData += "<td class='Heading'>" + value.ProductImageHeading + "</td>";
            tableData += "<td class='imageName hidden'>" + value.ProductImageName + "</td>";
            tableData += "<td class='description'>" + value.Description + "</td>";
            tableData += "<td class='imgUrl hidden'>" + value.ProductImageUrl + "</td>";
            tableData += "<td class='image'><img src='data:image/jpg;base64," + value.ProductImage + "' width=300 height=250/></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#productTableBody").append(tableData);
}

