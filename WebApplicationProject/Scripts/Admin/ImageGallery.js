﻿$(document).ready(function () {

    GetAllGallery();
    //$("#saveBtn").click(SaveContact);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(Delete);
    $("#deleteBtn").hide();
    $("#message_box").hide();
    $("#image").change(function () {
        readURL(this);
        var selectedFile = $("#image")[0].files[0];
        selectedFile.convertToBase64(function (base64) {
            //alert(base64);
            baseStringImage = "";
            blobImage = "";
            blobImage = makeblob(base64);
            baseStringImage = base64;

        });
    });
});

//Global Variable
var fileName;
var baseStringImage = "";
var blobImage = "";
var autoRowId = 1;
var sliderGlobalId = 0;

//*************************Image Manage**************************************
// preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#prevImg").attr("src", "");
            $('#prevImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function makeblob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = decodeURIComponent(parts[1]);
        return new Blob([raw], { type: contentType });
    }
    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
File.prototype.convertToBase64 = function (callback) {
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };
    reader.readAsDataURL(this);
};
//************************************************************
function GetAllGallery() {
    $.ajax({
        url: "ImageGalleryHandler.ashx/ProcessRequest",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            LoadGalleryTable(data);

        },
        error: function (ex) {
            //alert("Hoy nai");
        }
    });
}

function LoadGalleryTable(data) {
    var tableData = "";
    $("#galleryTableBody").empty();
    $("#gridViewTable").DataTable().destroy();
    var SL = 1;
    var html = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='galleryListRow'id=gallery_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Title'>" + value.Title + "</td>";
            tableData += "<td class='imageName hidden'>" + value.ImageName + "</td>";
            tableData += "<td class='description'>" + value.Description + "</td>";
            tableData += "<td class='imgUrl hidden'>" + value.ImageUrl + "</td>";
            tableData += "<td class='image'><img src='data:image/jpg;base64," + value.ImageStringData + "' width=300 height=250/></td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#galleryTableBody").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}


// SAVE PRODUCT
function showMessage() {
    $("#message_box").html("Image is required").addClass("alert alert-warning alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
}
function SaveGallery() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);

    if ($("#saveBtn").text() == "Update") {
        UpdateSlider();
    }
    else {
        if (baseStringImage!="") {
        var imageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
        var imageName = baseStringImage.split(';')[0]; 
        }


        var imageTitle = $("#imageTitle").val();
        var description = $("#description").val();
        var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

        if (baseStringImage == "") {
            $("#saveBtn").prop("disabled", false);
            return showMessage();
        }

        var galleryObj = {
            Id: sliderGlobalId,
            Title: imageTitle,
            ImageStringData: baseStringImage,
            Description: description
        }
        $.ajax({
            url: "ImageGalleryHandler.ashx/ProcessRequest?ac=send",
            type: "POST",
            data: JSON.stringify({ galleryObj: galleryObj}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                var d = data;

                if (data > 0) {
                    GetAllGallery();
                    ClearForm();
                    $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                    $("#msgBox2").hide();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    alert("failed");
                    $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
            },
            error: function (ex) {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
            }
        });
    }
}

function Edit(id) {
    $("#prevImg").attr("src", "");
    var varId = $("#gallery_" + id).find("td.Id").text();
    sliderGlobalId = varId;
    
    $("#varId").val(varId);
    var img = $("#gallery_" + id).find("td.image").html();
    var imgSrc = $(img).attr('src');
    var url = $("#gallery_" + id).find("td.imgUrl").text();
    var imageName = $("#gallery_" + id).find("td.imageName").text();
    $("#imageTitle").val($("#gallery_" + id).find("td.Title").text());
    $("#description").val($("#gallery_" + id).find("td.description").text());

    $("#image input:file").val(url);
    $("#imgUrl").val(url);
    $("#imgName").val(imageName);
    $("#prevImg").attr("src", "");
    $("#prevImg").attr("src", imgSrc);


    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function UpdateSlider() {
    if (baseStringImage=="") {
        baseStringImage = $("#prevImg").attr('src');
    }

    var imageBase64 = baseStringImage.split(';')[1].replace("base64,", "");
    var imageName = baseStringImage.split(';')[0];

    var imageTitle = $("#imageTitle").val();
    var imgUrl = $("#imgUrl").val();
    var imgName = $("#imgName").val();
    var description = $("#description").val();
    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    //if (imageTitle == "" && description == "") {
    //    $("#saveBtn").prop("disabled", false);
    //    return showMessage();
    //}
    var galleryObj = {
        Id: sliderGlobalId,
        Title: imageTitle,
        ImageStringData: baseStringImage,
        ImageName: imgName,
        ImageUrl: imgUrl,
        Description: description
    }
    $.ajax({
        url: "ImageGalleryHandler.ashx/ProcessRequest?ac=send",
        type: "POST",
        data: JSON.stringify({ galleryObj: galleryObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function (data) {
            var d = data;
            if (data > 0) {
                GetAllGallery();
                ClearForm();
                $("#msgBox2").html("").removeClass("success").delay(5000).fadeOut();
                $("#msgBox2").hide();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                $("#saveBtn").text("Save");
                return;
            } else {
                $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
                $("#saveBtn").prop("disabled", false);
                return;
            }
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200).delay(5000).fadeOut();
        }
    });
}
function ClearForm() {
    sliderGlobalId = 0;
    baseStringImage = "";
    $("#varId").val(0);
    $("#image").val("");
    $("#imageTitle").val("");
    $("#imgUrl").val("");
    $("#imgName").val("");
    $("#description").val("");
    $("#prevImg").attr('src','');
    $("#saveBtn").prop("disabled", false);
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete() {
    var id = $("#varId").val();
    var imgUrl = $("#imgUrl").val();
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "ImageGalleryUI.aspx/DeleteAGallery",
            type: "POST",
            data: JSON.stringify({ Id: id, imgUrl: imgUrl }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    alert(data.d);
                    GetAllGallery();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
