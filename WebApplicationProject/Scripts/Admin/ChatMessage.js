﻿$(document).ready(function () {

    GetAllItemList();
    $("#saveBtn").click(SaveAbout);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(DeleteAbout);
    $("#deleteBtn").hide();
    $("#message_box").hide();
});

var autoRowId = 1;
var itemGlobalId = 0;

var htmlTextArray;
function GetAllItemList() {
    $.ajax({
        url: "ChatMessagesUI.aspx/GetAllItemList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            htmlTextArray = data.d;
            LoadChatTable(data.d);
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}


function LoadChatTable(data) {
    var tableData = "";
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='aboutListRow'id=message_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='QuestionCategory'>" + value.QuestionCategory + "</td>";
            tableData += "<td class='Question'>" + value.Question + "</td>";
            tableData += "<td class='PersonName'>" + value.PersonName + "</td>";
            tableData += "<td class='ContactType'>" + value.ContactType + "</td>";
            tableData += "<td class='Email'>" + value.Email + "</td>";
            tableData += "<td class='ContactNo'>" + value.ContactNo + "</td>";
            tableData += "<td><button type='button' class='btn btn-danger btn-sm' onclick='Delete(" + autoRowId + ")'>Delete</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#messageTableBody").empty();
    $("#gridViewTable").DataTable().destroy();
    $("#messageTableBody").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}
function Edit(id) {
    var messageId = $("#message_" + id).find("td.Id").text();
    var dataRow = htmlTextArray.filter(function (obj) {
        return (obj.Id == messageId);
    });

    $("#aboutId").val(messageId);

    //var QuestionCategory = $("#message_" + id).find("td.QuestionCategory").text();
    //var Question = $("#message_" + id).find("td.Question").text();
    //var PersonName = $("#message_" + id).find("td.PersonName").text();

    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    itemGlobalId = 0;
    $("#aboutId").val(0);
    CKEDITOR.instances.companyQuestionCategory.setData('');
    CKEDITOR.instances.Question.setData('');
    CKEDITOR.instances.PersonName.setData('');
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function Delete(id) {
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "About.aspx/DeleteAbout",
            type: "POST",
            data: JSON.stringify({ Id: id }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    alert(data.d);
                    GetAllItemList();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}





