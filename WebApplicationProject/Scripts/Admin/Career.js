﻿$(document).ready(function () {

    GetAllCareerList();
    $("#saveBtn").click(SaveCareer);
    $("#clearBtn").click(ClearForm);
    $("#deleteBtn").click(DeleteAbout);
    $("#deleteBtn").hide();
    $("#message_box").hide();
});

var autoRowId = 1;
var itemGlobalId = 0;

var htmlTextArray;
function GetAllCareerList() {
    $.ajax({
        url: "CareerUI.aspx/GetAllCareerList",
        type: "POST",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            htmlTextArray = data.d;
            LoadDataTable(data.d);
        },
        error: function (ex) {
            alert(ex.message);
        }
    });
}


function LoadDataTable(data) {
    var tableData = "";
    var SL = 1;
    if (data.length > 0) {
        $.each(data, function (key, value) {
            tableData += "<tr class='careerListRow'id=career_" + autoRowId + ">";
            tableData += "<td class='SL'>" + SL + "</td>";
            tableData += "<td class='Id hidden'>" + value.Id + "</td>";
            tableData += "<td class='Designation'>" + value.Designation + "</td>";
            tableData += "<td class='Requirement'>" + value.Requirement + "</td>";
            tableData += "<td class='Description'>" + value.Description + "</td>";
            tableData += "<td class='Address hidden'>" + value.Address + "</td>";
            tableData += "<td class='ContactPersonName hidden'>" + value.ContactPersonName + "</td>";
            tableData += "<td class='ContactPersonDetails hidden'>" + value.ContactPersonDetails + "</td>";
            tableData += "<td class='Mobile'>" + value.Mobile + "</td>";
            tableData += "<td class='Phone'>" + value.Phone + "</td>";
            tableData += "<td class='Email'>" + value.Email + "</td>";
            tableData += "<td class='ApplicationStartDate hidden'>" + value.ApplicationStartDate + "</td>";
            tableData += "<td class='ApplicationLastDate'>" + value.ApplicationLastDate + "</td>";
            tableData += "<td><button type='button' class='btn btn-info btn-sm' onclick='Edit(" + autoRowId + ")'>Edit</button></td>";
            tableData += "</tr>";
            SL++;
            autoRowId++;
        });
    }
    $("#aboutTableBody").empty();
    $("#gridViewTable").DataTable().destroy();
    $("#aboutTableBody").append(tableData);
    $('#gridViewTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': false,
        'info': true,
        'autoWidth': false
    });
}

function showMessage() {
    $("#message_box").html("Designation, Email, Mobile, Address, Requirement and Description Requireds").addClass("alert alert-danger alert-dismissible").fadeIn(1000).delay(5000).fadeOut();
    $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
}
function SaveCareer() {
    $("#msgBox2").html("Working").addClass("success").show(200);
    $("#saveBtn").prop("disabled", true);
    var designation = $("#designation").val();
    var applicationStartDate = $("#applicationStartDate").val();
    var applicationLastDate = $("#applicationLastDate").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var phone = $("#phone").val();
    var contactPersonDetails = $("#contactPersonDetails").val();
    var contactPersonName = $("#contactPersonName").val();
    var address = $("#address").val();
    var requirement = CKEDITOR.instances['requirement'].getData();
    var description = CKEDITOR.instances['description'].getData();
 
    var id = $("#varId").val() == "" || $("#varId").val() == null || $("#varId").val() == 'NaN' ? 0 : parseInt($("#varId").val());

    if (designation == "" || email == "" || mobile == "" || address == "" || requirement == "" || description == "") {
        $("#saveBtn").prop("disabled", false);
        return showMessage();
    }

    var careerObj = {
        Id: id,
        Designation: designation,
        Requirement: requirement,
        Description: description,
        Address: address,
        ContactPersonName: contactPersonName,
        ContactPersonDetails: contactPersonDetails,
        Mobile: mobile,
        Phone: phone,
        Email: email,
        ApplicationStartDate: applicationStartDate,
        ApplicationLastDate: applicationLastDate
    }
    $.ajax({
        url: "CareerUI.aspx/SaveCareer",
        type: "POST",
        data: JSON.stringify({ careerObj: careerObj }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            GetAllCareerList();
            if (data.d == "Updated") {
                ClearForm();
                $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
                $('#message_box').html("Updated").addClass("alert alert-success alert-dismissible").show(200);
                $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                }
                else if (data.d == "Failed") {
                    $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
                    //$("#msgBox2").hide();
                    $('#message_box').html("Failed").addClass("alert alert-Danger alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    $("#saveBtn").prop("disabled", false);
                    return;
                } else {
                    if (data.d == "Saved") {
                        ClearForm();
                        $("#msgBox2").html("Working").addClass("success").show(200).delay(5000).fadeOut();
                        //$("#msgBox2").hide();
                        $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                        $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                        $("#saveBtn").prop("disabled", false);                        
                        return;
                    }
                }           
        },
        error: function (ex) {
            $("#msgBox2").html("Failed").addClass("error").show(200);
        }
    });
}

function Edit(id) {
    var varId = $("#career_" + id).find("td.Id").text();
    var dataRow = htmlTextArray.filter(function (obj) {
        return (obj.Id == varId);
    });

    $("#varId").val(varId);
    $("#designation").val($("#career_" + id).find("td.Designation").text());
    $("#applicationStartDate").val($("#career_" + id).find("td.ApplicationStartDate").text());
    $("#applicationLastDate").val($("#career_" + id).find("td.ApplicationLastDate").text());
    $("#email").val($("#career_" + id).find("td.Email").text());
    $("#mobile").val($("#career_" + id).find("td.Mobile").text());
    $("#phone").val($("#career_" + id).find("td.Phone").text());
    $("#contactPersonName").val($("#career_" + id).find("td.ContactPersonName").text());
    $("#contactPersonDetails").val($("#career_" + id).find("td.ContactPersonDetails").text());
    $("#address").val($("#career_" + id).find("td.Address").text());

    CKEDITOR.instances.requirement.setData(dataRow[0].Requirement);
    CKEDITOR.instances.description.setData(dataRow[0].Description);

    $("#saveBtn").text('Update');
    $("#deleteBtn").show();
}

function ClearForm() {
    itemGlobalId = 0;
    $("#varId").val(0);
    CKEDITOR.instances.requirement.setData('');
    CKEDITOR.instances.description.setData('');
    $("#designation").val("");
    $("#applicationStartDate").val("");
    $("#applicationLastDate").val("");
    $("#email").val("");
    $("#mobile").val("");
    $("#phone").val("");
    $("#contactPersonName").val("");
    $("#contactPersonDetails").val("");
    $("#address").val("");
    $("#saveBtn").text('Save');
    $("#deleteBtn").hide();

}

function DeleteAbout() {
    var x = confirm("Are you sure to delete?");
    if (x == true) {
        $.ajax({
            url: "CareerUI.aspx/DeleteCareer",
            type: "POST",
            data: JSON.stringify({ Id: $("#varId").val() }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d == "Deleted") {
                    $("#msgBox2").removeClass("success").fadeOut();
                    $('#message_box').html("Saved").addClass("alert alert-success alert-dismissible").show(200);
                    $('#message_box').fadeIn(1000).delay(5000).fadeOut();
                    GetAllCareerList();
                    ClearForm();
                }

            },
            error: function (ex) {
                alert("There are no data");
            }
        });
    }
}
